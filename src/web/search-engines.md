# Search Engines

[DuckDuckGo](https://start.duckduckgo.com/)
<a href="https://www.linuxmint.com/searchengines/anse.php?sen=DuckDuckGo"><img src="../Media/m/firefox.webp" alt="" loading="lazy" class="emoji"></a>

[Google](https://www.google.com/)
<a href="https://www.linuxmint.com/searchengines/anse.php?sen=Google&c=y"><img src="../Media/m/firefox.webp" alt="" loading="lazy" class="emoji"></a>

[Startpage](https://www.startpage.com/)
<a href="https://www.linuxmint.com/searchengines/anse.php?sen=Startpage"><img src="../Media/m/firefox.webp" alt="" loading="lazy" class="emoji"></a>

[Qwant](https://www.qwant.com/)
<a href="https://www.linuxmint.com/searchengines/anse.php?sen=Qwant&c=y"><img src="../Media/m/firefox.webp" alt="" loading="lazy" class="emoji"></a>
