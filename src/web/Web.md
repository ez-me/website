# Web

- [Reverse Image Search](./revimgsearch.md)
- [File Transfer](./transfer.md)
- [URL Shorteners](./short-url.md)
- [Online Compression](./compress.md)
- [Anti Malware](./anti-malware.md)
- [Search Engines](./search-engines.md)
- [Web Extensions](./extensions.md)
- [AI Manipulators](./ai-manipulators.md)
