# AI Manipulators

## Images

### Upscale

[waifu2x](https://www.waifu2x.net/)
<i>Enlarge anime style digital drawings.</i>

[Bigjpg](https://bigjpg.com/)
<i>Mostly for anime style digital drawings.</i>

[jpgHD](https://jpghd.com/)
<i>Attemps to restore old photographs.</i>

<!--- Unsure
[jpgHi](https://jpghi.com/)
<i>Alters photographs more dramatically.</i>
--->

### Modify

[jpgRM](https://jpgrm.com/)
<i>Remove items from images with AI healing.</i>

[IDjpg](https://idjpg.com/)
<i>Change the artstyle of Pictures.</i>

<!--- Not sure if I want to add this...
### Create

[Craiyon](https://www.craiyon.com/)
<i>Create pictures</i>
--->

## Videos

[bigmp4](https://bigmp4.com/)
<i>Upscale, and Interpolate videos.</i>
