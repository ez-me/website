# File Transfer

## Direct

<a href="http://xkcd949.com/" target="_blank" rel="noopener noreferrer">
 xkcd949</a>
<br><br>

<a href="https://justbeamit.com/" target="_blank" rel="noopener noreferrer">
 Just Beam It</a>
<br><br>

<a href="https://sharedrop.io/" target="_blank" rel="noopener noreferrer">
 ShareDrop</a>
<br>


## Download Later

|Site|Size|Duration|
|---|---|---|
|<a href="https://anontransfer.com/" target="_blank" rel="noopener noreferrer"> AnonTransfer</a>|15 GB|30 Days|
|<a href="https://easyupload.io/" target="_blank" rel="noopener noreferrer"> Easyupload.io</a>|10 GB|30 Days|
|<a href="https://dailyuploads.net/" target="_blank" rel="noopener noreferrer"> Daily Uploads</a>|?|?|
|<a href="https://fastupload.io/" target="_blank" rel="noopener noreferrer"> Fastupload</a>|50GB|30 Days|
|<a href="https://filebin.net/" target="_blank" rel="noopener noreferrer"> FileBin</a>|?|6 Days|
|<a href="https://fileditch.com/" target="_blank" rel="noopener noreferrer"> Fileditch</a>|1 GB|Permanent?|
|<a href="https://fireload.com/" target="_blank" rel="noopener noreferrer"> Fireload</a>|2 GB|58 Days|
|<a href="https://gofile.io/" target="_blank" rel="noopener noreferrer"> Gofile</a>|Unlimited?|Permanent?|
|<a href="https://hexload.com/" target="_blank" rel="noopener noreferrer"> HexUpload</a>|15 GB|30 Days|
|<a href="https://krakenfiles.com/" target="_blank" rel="noopener noreferrer"> KrakenFiles</a>|1 GB|3 Months|
|<a href="https://megaup.net/" target="_blank" rel="noopener noreferrer"> MegaUp</a>|5 GB|60 Days|
|<a href="https://send.now/" target="_blank" rel="noopener noreferrer"> Send now</a>|30 GB|30 Days|
|<a href="https://wormhole.app/" target="_blank" rel="noopener noreferrer"> Wormhole</a>|5 GB|1 Day|

<br>

### Command line

|Site|Size|Duration|
|---|---|---|
|<a href="http://0x0.st/" target="_blank" rel="noopener noreferrer"> The null pointer</a>|512 MiB|Depends|
|<a href="https://0.vern.cc/" target="_blank" rel="noopener noreferrer"> ~vern null pointer</a>|5 GiB|Permanent?|
|<a href="https://blog.orhun.dev/blazingly-fast-file-sharing/#blazingly-fast-file-sharing-rustypaste-crab" target="_blank" rel="noopener noreferrer"> Rustypaste</a>|?|?|
|<a href="https://share.schollz.com/" target="_blank" rel="noopener noreferrer"> Schollz share</a>|500 MB|Depends|

<!--- down?
|<a href="https://transfer.sh/" target="_blank" rel="noopener noreferrer"> transfer.sh</a>|10 GB|14 Days|
--->

<br>

## Cloud Storage

|Site|Size|
|---|---|
|<a href="https://clicknupload.click/" target="_blank" rel="noopener noreferrer"> Clicknupload</a>|Unlimited?|
|<a href="http://mega.io/" target="_blank" rel="noopener noreferrer"> MEGA</a>|25 GB|
|<a href="https://qiwi.gg/" target="_blank" rel="noopener noreferrer"> Qiwi</a>|?|


