# Online Compression

## General use

<a href="https://www.wecompress.com/" target="_blank" rel="noopener noreferrer">
 WeCompress</a>
<br><br>

<a href="https://compress-online.com/" target="_blank" rel="noopener noreferrer">
 Compress-Online</a>
<br><br>

<a href="https://www.compress2go.com/" target="_blank" rel="noopener noreferrer">
 Compress2Go</a>
<br><br>

<a href="https://www.youcompress.com/" target="_blank" rel="noopener noreferrer">
 YouCompress</a>
<br>


## PDF

### Only compress

<a href="https://shrinkpdf.com/" target="_blank" rel="noopener noreferrer">
 ShrinkPDF</a>
<br>

### Full editing suite

<a href="https://www.adobe.com/acrobat/online/compress-pdf.html" target="_blank" rel="noopener noreferrer">
 Adobe Acrobat PDF Online</a>
<br><br>

<a href="https://www.ilovepdf.com/compress_pdf" target="_blank" rel="noopener noreferrer">
 ILovePDF</a>
<br><br>

<a href="https://pdf.io/compress/" target="_blank" rel="noopener noreferrer">
 PDF.io</a>
<br>

## General converter site

<a href="https://www.online-convert.com/document-compressor/compress-pdf" target="_blank" rel="noopener noreferrer">
 Online-Convert PDF</a>
<br><br>

<a href="https://cloudconvert.com/compress-pdf" target="_blank" rel="noopener noreferrer">
 CloudConvert PDF</a>
<br>


## Images

<a href="https://imageoptim.com/online" target="_blank" rel="noopener noreferrer">
 ImageOptim</a>
<br><br>

<a href="https://compresspng.com/" target="_blank" rel="noopener noreferrer">
 CompressPNG</a>
<br><i>has sister sites with other formats</i>
<br><br>

<a href="https://ezgif.com/optiwebp" target="_blank" rel="noopener noreferrer">
 Ezgif WebP</a>
<br><i>also has other formats</i>
<br>


## Video

<a href="https://video-converter.com/" target="_blank" rel="noopener noreferrer">
 123aps Video Converter</a>
<br><br>

<a href="https://studio.youtube.com/" target="_blank" rel="noopener noreferrer">
 YouTube</a>
<br><i>You can't upload copyrighted or innapropiate material.</i>
<br>


