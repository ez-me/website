# Web Extensions

<!--- base
<a href="">
 </a>
<a href="https://addons.mozilla.org/firefox/addon//"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>
--->

## AdBlock

<a href="https://github.com/gorhill/uBlock/">
<img src="../Media/m/ubo.webp" alt="🛡" loading="lazy" class="icon"> uBlock Origin</a>
<a href="https://addons.mozilla.org/firefox/addon/ublock-origin/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<br><br>

<a href="https://github.com/uBlockOrigin/uBOL-home/">
<img src="../Media/m/ubo-lite.webp" alt="🛡" loading="lazy" class="icon"> uBO Lite</a>
<a href="https://chromewebstore.google.com/detail/ddkjiahejlhfcafbddmgiahcphecmpfh"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<a href="https://fastforward.team/">
<img src="../Media/m/fastforward.webp" alt="🔷" loading="lazy" class="icon"> FastForward</a>
<a href="https://addons.mozilla.org/firefox/addon/fastforwardteam/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/icallnadddjmdinamnolclfjanhfoafe"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br>


## Security

<a href="https://emsisoft.com/en/help/1974/emsisoft-browser-security/">
<img src="../Media/m/emsisoft.webp" alt="🛡" loading="lazy" class="icon"> Emsisoft Browser Security</a>
<a href="https://addons.mozilla.org/firefox/addon/emsisoft-browser-security/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/jfofijpkapingknllefalncmbiienkab"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<a href="https://noscript.net/">
<img src="../Media/m/noscript.webp" alt="🚫" loading="lazy" class="icon"> NoScript Security Suite</a>
<a href="https://addons.mozilla.org/firefox/addon/noscript/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/doojmbjmlfjjnbmnoijecmcbfeoakpjm"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br>


## CDN Cache

<a href="https://localcdn.org/">
<img src="../Media/m/LocalCDN.webp" alt="🛡" loading="lazy" class="icon"> LocalCDN</a>
<a href="https://addons.mozilla.org/firefox/addon/localcdn-fork-of-decentraleyes/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/njdfdhgcmkocbgbhcioffdbicglldapd"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br>


## Redirects

<!--- ↪️ --->
<a href="https://getindie.wiki/">
 Indie Wiki Buddy</a>
<a href="https://addons.mozilla.org/firefox/addon/indie-wiki-buddy/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/fkagelmloambgokoeokbpihmgpkbgbfm"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<!--- 🇼 --->
<a href="https://wikiwand.com/">
 Wikiwand</a>
<a href="https://addons.mozilla.org/firefox/addon/wikiwand-wikipedia-modernized/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/emffkefkbkpkgpdeeooapgaicgmcbolj"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<!--- ↗️ --->
<a href="https://libredirect.github.io/">
 LibRedirect</a>
<a href="https://addons.mozilla.org/firefox/addon/libredirect/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://libredirect.github.io/download_chromium.html"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br>


## YouTube

<!--- 🛡️ --->
<a href="https://sponsor.ajay.app/">
 SponsorBlock</a>
<a href="https://addons.mozilla.org/firefox/addon/sponsorblock/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/mnjggcdmjocbbbhaepdhchncahnbgone"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<!--- 🧿 --->
<a href="https://dearrow.ajay.app/">
 DeArrow</a>
<a href="https://addons.mozilla.org/firefox/addon/dearrow/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/enamippconapkdmgfgjchkhakpfinmaj"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<!--- 👎 --->
<a href="https://returnyoutubedislike.com/">
 Return YouTube Dislike</a>
<a href="https://addons.mozilla.org/firefox/addon/return-youtube-dislikes/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/gebbhagfogifgggkldgodflihgfeippi"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<!--- ▶️ --->
<a href="https://github.com/code-charity/youtube/">
 Improved Youtube</a>
<a href="https://addons.mozilla.org/firefox/addon/youtube-addon/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/bnomihfieiccainjcjblhegjgglakjdd"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br>


## Twitch

<!--- 🐶 --->
<a href="https://frankerfacez.com/">
 FrankerFaceZ</a>
<a href="https://frankerfacez.com/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/fadndhdgpmmaapbmfcknlfgcflmmmieb"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<!--- 😀 --->
<a href="https://betterttv.com/">
 BetterTTV</a>
<a href="https://addons.mozilla.org/firefox/addon/betterttv/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/ajopnjidmegmdimjlfnijceegpefgped"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br>


## Git

<!--- 📁 --->
<a href="https://github.com/Claudiohbsantos/github-material-icons-extension/">
 Material Icons</a>
<a href="https://addons.mozilla.org/firefox/addon/material-icons-for-github/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/bggfcpfjbdkhfhfmkjpbhnkhnpjjeomc"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<!--- 🐙 --->
<a href="https://github.com/refined-github/refined-github/">
 Refined Github</a>
<a href="https://addons.mozilla.org/firefox/addon/refined-github-/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/hlepfoohegkhhmjieoechaddaejaokhf"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br>


## Other

<!--- ↗️ --->
<a href="https://tanalin.com/en/projects/smart-upscale/">
 Smart Upscale</a>
<a href="https://addons.mozilla.org/firefox/addon/smart-upscale/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/cafgibgoaehhjoomjcndeogbcmfdbogd"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<!--- 🟪 --->
<a href="https://github.com/dessant/web-archives">
 Web Archives</a>
<a href="https://addons.mozilla.org/firefox/addon/view-page-archive/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/hkligngkgcpcolhcnkgccglchdafcnao"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<!--- 🐵 --->
<a href="https://violentmonkey.github.io/">
 Violentmonkey</a>
<a href="https://addons.mozilla.org/firefox/addon/violentmonkey/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/jinjaccalgkegednnccohejagnlnfdag"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<!--- 🔘 --->
<a href="https://github.com/FiftyNine/SCPper/">
 SCPper</a>
<a href="https://addons.mozilla.org/firefox/addon/scpper-for-firefox/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/cpebggobaenfndpoddkdindknnpbjdfc"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>

<!--- 🚫 --->
<a href="https://iorate.github.io/ublacklist/">
 uBlacklist</a>
<a href="https://addons.mozilla.org/firefox/addon/ublacklist/"><img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="emoji"></a>
<a href="https://chromewebstore.google.com/detail/pncfbmialoiaghdehhbnbhkkgmjanfhe"><img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="emoji"></a>
<br><br>
