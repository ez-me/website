# URL Shorteners

## fully random

<a href="https://shorturl.at/" target="_blank" rel="noopener noreferrer">
 ShortUrl</a>
<br><br>

<!--- dead?
<a href="http://bombch.us/" target="_blank" rel="noopener noreferrer">
 Bombchus</a>
<br>
--->


## anon custom

<a href="https://is.gd/" target="_blank" rel="noopener noreferrer">
 is good</a>
<br><br>

<a href="https://tiny.cc/" target="_blank" rel="noopener noreferrer">
 Tinycc</a>
<br><br>

<a href="https://tinyurl.com/" target="_blank" rel="noopener noreferrer">
 TinyURL</a>
<br>


## account custom

<a href="https://bitly.com/pages/products/url-shortener" target="_blank" rel="noopener noreferrer">
 Bitly</a>
<br>


## paid custom

<a href="https://free-url-shortener.rb.gy/" target="_blank" rel="noopener noreferrer">
 RB.GY</a>
<br><br>

<a href="https://cutt.ly/" target="_blank" rel="noopener noreferrer">
 Cuttly</a>
<br><br>

<a href="https://shorturl.gg/" target="_blank" rel="noopener noreferrer">
 Simple URL Shortener</a>
<br>
