# Magisk Installation

## Prerequisites

You need to have the following programs:

<code>[platform-tools (adb, fastboot)](https://developer.android.com/studio/releases/platform-tools)</code> and <code>[python](https://www.python.org/downloads/)</code>.


## Getting the files

You need a copy of your <code>boot.img</code> file, which is sometimes distributed alongside the main Custom ROM, but is also available in the <code>payload.bin</code> in the installation ZIP (we need to use Python to extract it!).

If you have your <code>boot.img</code> file already, only extract the <code>vbmeta.img</code> file.

### Install payload_dumper

````bash
python3 -m pip install pipx
````
````bash
python3 -m pipx install payload_dumper
````

<i>Why install pipx and then payload_dumper? Because pipx is better for installing isolated python modules automatically and reliably.</i>

### Extract the IMG files

````bash
payload_dumper --partitions boot,vbmeta payload.bin
````


## Patching

Copy the <code>boot.img</code> to your phone, it's small enough to go fast with MTP or FTP.

Install the official [Magisk APK](https://github.com/topjohnwu/Magisk/releases/latest) on your device, and click Install, that will lead you to patching your file.

If you are feeling adventurous, you could try unnoficial forks like [Alpha](https://install.appcenter.ms/users/vvb2060/apps/magisk/distribution_groups/public/), or just the official [Canary](https://github.com/topjohnwu/magisk-files/raw/canary/app-release.apk).

After it finishes, copy it back to the computer.


## Flashing

Boot into fastboot/download/bootloader mode, an easy way is by
````bash
adb reboot bootloader
````

Now just flash the modified <code>boot.img</code> and the <code>vbmeta.img</code> in a special way.

````bash
fastboot flash boot magisk_patched-$VERSION_$RANDOM.img
````

````bash
fastboot flash vbmeta --disable-verity --disable-verification vbmeta.img
````


## Finishing notes

Reboot into Android, and you should see the Magisk APK telling you now have root and can install <a href="./magisk-modules.html">Modules!</a>.

