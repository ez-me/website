# Xposed Modules


## Quality of Life improvements

[Automatic Avanced Settings Expander](https://github.com/binarynoise/XposedModulets/releases?q=AutomaticAdvancedSettingsExpander)

[Better Bluetooth Device Sort](https://github.com/binarynoise/XposedModulets/releases?q=betterBluetoothDeviceSort)

[SMS Code](https://modules.lsposed.org/module/com.github.tianma8023.xposed.smscode)

[Maps Tweaks](https://modules.lsposed.org/module/ru.mike.mapstweaks)

[Tarnhelm](https://modules.lsposed.org/module/cn.ac.lz233.tarnhelm)


## User Interface

[Mobile Icon Changer](https://modules.lsposed.org/module/com.programminghoch10.mobileiconchanger)

[Notification Shade Transparency](https://modules.lsposed.org/module/com.asyjaiz.a12blur)

[Location Indicator Whitelist](https://modules.lsposed.org/module/fr.netstat.locationindicatorwhitelist)

[QuoteLockX](https://modules.lsposed.org/module/com.yubyf.quotelockx)

[Installer++](https://modules.lsposed.org/module/ltd.nextalone.pkginstallerplus)


## Advanced

[Enable Screenshot](https://modules.lsposed.org/module/io.github.lsposed.disableflagsecure)

[Bypass Root Check Pro](https://modules.lsposed.org/module/com.gauravssnl.bypassrootcheck.pro)

[FakeGApps](https://modules.lsposed.org/module/inc.whew.android.fakegapps)

[Bootloader Spoofer](https://modules.lsposed.org/module/es.chiteroman.bootloaderspoofer)

[Update Locker](https://modules.lsposed.org/module/ru.mike.updatelocker)
