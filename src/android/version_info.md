# Version info

## API Levels
[API Levels](https://apilevels.com/)
This helps knowing at a glance which versions are being used and supported.

## Update Tracker
[Android Update Tracker](https://androidupdatetracker.com/)
This helps knowing how your phone is getting updates officialy.

