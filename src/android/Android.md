# Android

- [Version info](version_info.md)
- [ROMs](roms.md)
- [ADB PM Help](adb_pm.md)
- [WiFi ADB](wifi_adb.md)
- [Magisk Modules](magisk-modules.md)
- [Xposed Modules](xposed-modules.md)
- Tutorials
    - [Magisk Installation](magisk.md)
