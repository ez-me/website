# Magisk Modules

<i>Some require Zygisk to be enabled.</i>


## General

[Simple Bootloop Saver](https://github.com/Magisk-Modules-Alt-Repo/Simple_BootloopSaver/)

[1Controller](https://github.com/Magisk-Modules-Alt-Repo/OneController/)

[liboemcrypto disabler](https://github.com/Magisk-Modules-Repo/liboemcryptodisabler/)


## Battery

[Advanced Charging Controller](https://github.com/VR-25/acc/)

[GMS Doze](https://github.com/Magisk-Modules-Alt-Repo/GMSDoze/)


## Personalization

[LSPosed Framework](https://github.com/LSPosed/LSPosed/)

[Twemoji replacer](https://github.com/Magisk-Modules-Alt-Repo/ttf-twemoji/)


## Audio

[Audio Misc Settings](https://github.com/Magisk-Modules-Alt-Repo/audio-misc-settings/)

[DRC Remover](https://github.com/Magisk-Modules-Alt-Repo/drc-remover/)

[USB Samplerate Unlocker](https://github.com/Magisk-Modules-Alt-Repo/usb-samplerate-unlocker/)

[Audio Jitter Silencer](https://github.com/Magisk-Modules-Alt-Repo/audio-jitter-silencer/)

[Audio Compatibility Patch](https://github.com/Magisk-Modules-Repo/acp/)

[Audio Modification Library](https://github.com/Zackptg5/Audio-Modification-Library/)


## Privacy

[A-GPS SUPL replacer](https://github.com/Magisk-Modules-Alt-Repo/supl-replacer)

[DRM Disabler](https://github.com/Magisk-Modules-Alt-Repo/Magisk-DRM-Disabler)

[App Data Isolation](https://gitlab.com/adrian.m.miller/appdataisolation)


## Performance

[Yet Another Kernel Tweaker](https://github.com/Magisk-Modules-Alt-Repo/YAKT/)

[RiProG-AI](https://github.com/RiProG-id/RiProG-AI)

[Space saving Ahead-of-Time](https://github.com/Magisk-Modules-Alt-Repo/ezme-dex-space)

[Unity Fix](https://github.com/Magisk-Modules-Alt-Repo/unityfix)

[Automatic SQLite DB Optimizer](https://gitlab.com/adrian.m.miller/automaticsqlitedboptimizer)


## SafetyNet

[Universal SafetyNet & Play Integrity Fix [MOD]](https://github.com/Displax/safetynet-fix/)

[Play Integrity Fork](https://github.com/osm0sis/PlayIntegrityFork)

[Shamiko](https://github.com/LSPosed/LSPosed.github.io/releases/latest/)

[Zygisk-Assistant](https://github.com/snake-4/Zygisk-Assistant)

[Hide UserDebug, Test-Keys and LineageOS](https://github.com/Magisk-Modules-Alt-Repo/ezme-nodebug/)

[Sensitive Props](https://github.com/Magisk-Modules-Alt-Repo/sensitive_props/)

[xmlpak-RE](https://github.com/rushiranpise/xmlpak-RE/)

[Tricky Store](https://github.com/5ec1cff/TrickyStore)

