# Android ROMs

|Name|Version|≈Devices|
|---|---|---|
|<a href="https://calyxos.org"><img src="../Media/a/calyx.webp" alt="✊" loading="lazy" class="icon"></a> <a href="https://calyxos.org/install/">CalyxOS</a>|U|30|
|<a href="https://carbonrom.org/"><img src="../Media/a/carbon.webp" alt="🇨" loading="lazy" class="icon"></a> <a href="https://get.carbonrom.org/">CarbonROM</a>|R|50|
|<a href="https://crdroid.net"><img src="../Media/a/crdroid.webp" alt="👁️" loading="lazy" class="icon"></a> <a href="https://crdroid.net/downloads">crDroid</a>|U|120|
|<a href="https://derpfest.org/"><img src="../Media/a/derpfest.webp" alt="🦉" loading="lazy" class="icon"></a> <a href="https://sourceforge.net/projects/derpfest/files/">DerpFest</a>|T|70|
|<a href="https://grapheneos.org/"><img src="../Media/a/grapheneos.webp" alt="⚫" loading="lazy" class="icon"></a> <a href="https://grapheneos.org/releases">GrapheneOS </a>|U|21|
|<a href="https://havoc-os.com/"><img src="../Media/a/havoc-os.webp" alt="🚫" loading="lazy" class="icon"></a> <a href="https://havoc-os.com/download">Havoc-OS</a>|T|20|
|<a href="https://lineageos.org/"><img src="../Media/a/lineageos.webp" alt="🦑" loading="lazy" class="icon"></a> <a href="https://download.lineageos.org/">LineageOS</a>|U|200|
|<a href="https://lineage.microg.org/"><img src="../Media/a/los-ug.webp" alt="🐙" loading="lazy" class="icon"></a> <a href="https://download.lineage.microg.org/">LineageOS for microG</a>|U|200|
|<a href="https://omnirom.org/"><img src="../Media/a/omnirom.webp" alt="🟩" loading="lazy" class="icon"></a> <a href="https://dl.omnirom.org/">OmniROM</a>|U|5|
|<a href="https://paranoidandroid.co/"><img src="../Media/a/aospa.webp" alt="🅰️" loading="lazy" class="icon"></a> <a href="https://paranoidandroid.co/downloads">Paranoid Android</a>|U|70|
|<a href="https://pixelexperience.org"><img src="../Media/a/pixel-experience.webp" alt="🅿️" loading="lazy" class="icon"></a> <a href="https://download.pixelexperience.org/devices">Pixel Experience</a>|T|110|
|<a href="https://pixelos.net/"><img src="../Media/a/pixelos.webp" alt="🅿️" loading="lazy" class="icon"></a> <a href="https://pixelos.net/download">PixelOS</a>|U|60|

<!--- dead? abandoned?
|<a href="https://arrowos.net"><img src="../Media/a/arrowos.webp" alt="🔼" loading="lazy" class="icon"></a> <a href="https://arrowos.net/download">ArrowOS</a>|T|100|
|<a href="https://resurrectionremix.com/"><img src="../Media/a/resurrection.webp" alt="🪽" loading="lazy" class="icon"></a> <a href="https://sourceforge.net/projects/resurrectionremix-ten/files/">Resurrection Remix</a>|Q|80|
|<a href="https://syberiaos.com/"><img src="../Media/a/syberiaos.webp" alt="⛰️" loading="lazy" class="icon"></a> <a href="https://syberiaos.com/downloads">Syberia OS</a>|U|30|
--->

The letters, what do they mean? <br>
<code>9P</code>, <code>10Q</code>, <code>11R</code>, <code>12S</code>, <code>13T</code>, <code>14U</code>, <code>15V</code>, <code>16B</code>.

<br>

## Bonus, Google Apps

<a href="https://nikgapps.com/">
<img src="../Media/a/nikgapps.webp" alt="🇳" loading="lazy" class="icon"></a>
<a href="https://nikgapps.com/downloads">NikGApps</a>
<a href="https://sourceforge.net/projects/nikgapps/files/Releases/">⬇️</a>
<br><br>

<a href="https://microg.org/">
<img src="../Media/a/microg.webp" alt="🇨" loading="lazy" class="icon"></a>
<a href="https://microg.org/download.html">microG</a>
<br>

<!---
<a href="https://gitlab.com/MindTheGapps/vendor_gapps">
<img src="../Media/a/lineageos.webp" alt="🦑" loading="lazy" class="icon"></a> 
<a href="https://androidfilehost.com/?w=files&flid=322935">MindTheGapps</a>
<a href="http://downloads.codefi.re/jdcteam/javelinanddart/gapps">⬇️</a>
<br><br>
--->
