# Wifi ADB

<i>Make sure only <strong>one</strong> device is connected at a time.</i>

## Enable TCP/IP mode

```bash
adb tcpip 55555
```

## Connect to the phone

<i>The easiest way to know the local IP address of your phone is to look at the wifi settings of the current network, it should be like 192.168.0.100 or similar.</i>

```bash
adb connect 192.168.0.100:55555
```

Now you can use adb wirelessly!
