# ADB PM help

## AndroidDebugBridge PackageManager

## How to use?

````bash
adb shell
````

<i>You can use the following commands after this one. To exit type “exit”.</i>

<pre>
</pre>

````bash
adb shell $command
````

<i>Change $command for any of the ones below, you wont be placed in a different shell.</i>

<br>

## List Applications

````bash
pm list packages -e $type
````

<code>-s</code> : system

<code>-3</code> : manually installed

<br>

## Disable Applications

````bash
pm disable-user $package
````
<br>

````bash
pm suspend $package
````

<i>"Suspend" may be disabled after a (few) reboot(s), shows a Digital Wellbeing prompt.</i>

<br>

## Change permissions

````bash
pm grant $package $permission
````

````bash
pm revoke $package $permission
````

<i>Want the master list of permissions? <a href="https://developer.android.com/reference/android/Manifest.permission#summary">go Here</a></i>

<br>

## Optimizations

````bash
pm compile -a -m $type
````

<code>space</code> : reduce memory usage

<code>speed</code> : make apps faster

<pre>
</pre>

````bash
pm trim-caches 999999999999999999
````

<i>You can repeat this command many times, the longer it takes to execute, the more cache it removed.</i>

