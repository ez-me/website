# Emulators

<!---
https://emulation.gametechwiki.com/index.php/Main_Page
--->

## Table of Contents

[Multisystem Emulators](#multisystem-emulators)

[Nintendo](#nintendo)

[Sony](#sony)

[Sega](#sega)

[Microsoft](#microsoft)

[Atari](#atari)

<br>

## Multisystem Emulators

<a href="https://retroarch.com/">
<img src="../Media/m/retroarch.webp" alt="👾" loading="lazy" class="icon"> RetroArch</a>
 is the Frontend of <a href="https://libretro.com/">LibRetro</a>.
It generally has slightly out of date versions, but sometimes they have continued progress in otherwise abandoned emulators.
<br><br>

<a href="https://tasvideos.org/Bizhawk">
<img src="../Media/m/bizhawk.webp" alt="🦅" loading="lazy" class="icon"> Bizhawk</a>
 has some out of date cores, but some cores are Original.
It's mostly focused for the creation of Tool Assisted Speedruns (TAS), but can be used for casual play.
<br><br>

<a href="https://mednafen.github.io/">
<img src="../Media/m/mednafen.webp" alt="🐞" loading="lazy" class="icon"> Mednafen</a>
 has some out of date cores, but some cores are Original.
Since it's commandline for loading games, you can use <a href="https://github.com/AmatCoder/mednaffe/">Mednaffe</a> as GUI.
<br><br>

<a href="https://mamedev.org/">
<img src="../Media/m/mame.webp" alt="Ⓜ️" loading="lazy" class="icon"> MAME</a>
 is mostly focused on Arcade machines, with some good Home Console drivers.
An alternative GUI is <a href="https://sourceforge.net/projects/pfemame/">pfeMAME</a>.
Using it on RetroArch is... <a href="https://docs.libretro.com/guides/softwarelist-getting-started/">complicated</a>.
<br><br>

<a href="https://github.com/finalburnneo/FBNeo/">
<img src="../Media/m/fbneo.webp" alt="🔥" loading="lazy" class="icon"> FinalBurn Neo</a>
 is also focused on Arcade machines, with few Home Consoles.
Has more "Quality of Life" improvements compared to MAME, while being very similar in accuracy.
<br><br>

<a href="https://ares-emu.net/">
<img src="../Media/m/ares.webp" alt="🌙" loading="lazy" class="icon"> ares</a>
is the succesor of <a href="https://github.com/higan-emu/higan/">higan</a>, which is the superset of bsnes.
Most cores are original, with the oldest being the more accurate ones.
<br><br>


## Nintendo

### NES

|Emulator|Native|LibRetro|
|---|---|---|
|[Mesen](https://github.com/SourMesen/Mesen2/)|<a href="https://github.com/SourMesen/Mesen2/#development-builds"><img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="emoji">🐧</a>|[Yes<sup><abbr title="backported">*</abbr></sup>](https://docs.libretro.com/library/mesen/)|
|[Nestopia UE](http://0ldsk00l.ca/nestopia/)|<a href="http://0ldsk00l.ca/nestopia/"><img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="emoji">🐧</a>|[Yes](https://docs.libretro.com/library/nestopia_ue/)|

<code><a href="http://www.mediafire.com/download/35cpj3ghyxeq34o/FamicomDiskSystemBIOS.rar">fdd</a></code>

<br>

### SNES

|Emulator|Native|LibRetro|
|---|---|---|
|[Bsnes](https://github.com/bsnes-emu/bsnes/)|<a href="https://github.com/bsnes-emu/bsnes/releases"><img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="emoji">🐧🍎</a>|[Yes](https://github.com/libretro/bsnes-libretro/)|
|[bsnes-HD](https://github.com/DerKoun/bsnes-hd/)|<a href="https://github.com/DerKoun/bsnes-hd/releases"><img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="emoji">🐧🍎</a>|[Yes](https://github.com/libretro/bsnes-hd)|
|[Mesen](https://github.com/SourMesen/Mesen2/)|<a href="https://github.com/SourMesen/Mesen2/#development-builds"><img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="emoji">🐧</a>|[Yes<sup><abbr title="older version">*</abbr></sup>](https://docs.libretro.com/library/mesen-s/)|
|[Snes9x](http://snes9x.com/)|<a href="https://github.com/snes9xgit/snes9x/releases/latest"><img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="emoji">🐧🍎</a>|[Yes](https://docs.libretro.com/library/snes9x/)

<br>

### N64

|Emulator|LibRetro|
|---|---|
|[Rosalie's Mupen GUI](https://github.com/Rosalie241/RMG/)|No|
|[Simple64](https://simple64.github.io/)|No<sup><abbr title="Not yet">?</abbr></sup>|
|[Mupen64Plus-Next](https://parallel-launcher.ca/)|[Yes](https://docs.libretro.com/library/mupen64plus/)|
|[ParaLLEl N64](https://parallel-launcher.ca/)|[Yes](https://github.com/libretro/parallel-n64/)|

<code><a download href="https://64dd.org/dumps/N64DD_IPLROM_(J).zip">jp</a></code>
<code><a download href="https://64dd.org/dumps/64DD_IPL_US_MJR.n64">us</a></code>
<code><a download href="https://64dd.org/dumps/64DD_IPL_DEV_H4G.n64">dev</a></code>

<br>

### Gamecube / Wii

|Emulator|LibRetro|
|---|---|
|[Dolphin](https://dolphin-emu.org/)|[Yes](https://docs.libretro.com/library/dolphin/)|

<!---
|[PlayCube](https://github.com/emu-russia/pureikyubu/)|No|
--->

<code><a href="https://mediafire.com/file/2ajx3xr7v1ahqtf/GCN_BIOS.zip">ipl</a></code>
<code><a href="https://github.com/K11MCH1/AdrenoToolsDrivers/">drivers</a></code>
<code><a href="https://zadig.akeo.ie/">usb</a></code>

<br>

### Wii u

|Emulator|LibRetro|
|---|---|
|[Cemu](https://cemu.info/)|No|

<code><a href="https://mediafire.com/file/rdvtav576rtv28f/Wii%20U%20fonts.zip">fonts</a></code>
<code><a href="https://mediafire.com/file/cgcts0to3pya8g6/mlc01_WiiUMenu_5.5.2US.zip">menu</a></code>
<code><a href="https://zadig.akeo.ie/">usb</a></code>

<br>

### Switch

|Emulator|LibRetro|
|---|---|
|[GreemDev Ryujinx](https://github.com/GreemDev/Ryujinx/)|No|
|[Sudachi](https://sudachi.emuplace.app/)|No|
|[Strato](https://github.com/strato-emu/strato/)|No|

<code><a href="https://small.fileditchstuff.me/s13/JaXRNUTVDZbWTAyNPNYn.zip">keys</a></code>
<code><a href="https://darthsternie.net/switch-firmwares/">firmware</a></code>
<code><a href="https://github.com/K11MCH1/AdrenoToolsDrivers/">drivers</a></code>
<code><a href="https://zadig.akeo.ie/">usb</a></code>


<!--- Yuzu forks
|[suyu](https://suyu.dev/)|No|
|[Torzu](https://notabug.org/litucks/torzu/) [⬇️](https://free-git.org/Emulator-Archive/torzu/releases)|No|
--->

<!--- Ryujinx forks
|[Ryujinx-mirror](https://github.com/ryujinx-mirror/ryujinx/)|No|
|[KeatonTheBot Ryujinx](https://github.com/KeatonTheBot/Ryujinx/)|No|
|[Ryujinx-NX](https://github.com/Ryujinx-NX/Ryujinx)|No|
--->

<br>

### GameBoy / Color

|Emulator|LibRetro|
|---|---|
|[SameBoy](https://sameboy.github.io/)|[Yes](https://docs.libretro.com/library/sameboy/)|
|[Gambatte](https://github.com/pokemon-speedrunning/gambatte-speedrun/)|[Yes<sup><abbr title="Different fork">*</abbr></sup>](https://docs.libretro.com/library/gambatte/)|
|[BGB](https://bgb.bircd.org/)|No|
|[GBCC](https://gbcc.dev/)|No|

<!--- Mostly for obscure accesories
|[GB Enhanced+](https://github.com/shonumi/gbe-plus/)|No|
--->

<code><a download href="https://archive.org/download/dmg_rom/dmg_rom.bin">dmg</a></code>
<code><a download href="https://archive.org/download/cgb_boot/cgb_boot.bin">cgb</a></code>
<code><a href="https://mega.nz/#!tA01kCiI!Bp8p5BoaWzOKdF_m_V_stVcjT1TZVM-1gqaYU5uGqro">sgb</a></code>

<br>

### GameBoy Advance

|Emulator|LibRetro|
|---|---|
|[mGBA](https://mgba.io/)|[Yes](https://docs.libretro.com/library/mgba/)|
|[SkyEmu](https://skyemu.app/)|No|
|[NanoBoyAdvance](https://github.com/nba-emu/NanoBoyAdvance/)|No|

<code><a href="https://mediafire.com/file/uijj3i3349h8j2j/gba_bios.zip">bios</a></code>

<br>

### NDS / DSi

|Emulator|Native|LibRetro|
|---|---|---|
|[melonDS](https://melonds.kuribo64.net/)|<a href="https://melonds.kuribo64.net/downloads.php"><img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="emoji">🐧🍎</a> <a href="https://github.com/rafaelvcaetano/melonDS-android/releases/latest">🟩</a>|[Yes<sup><abbr title="Outdated">*</abbr></sup>](https://docs.libretro.com/library/melonds/)|
|[DeSmuME](https://desmume.org/)|<a href="https://desmume.org/download/"><img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="emoji"></a>|[Yes](https://docs.libretro.com/library/desmume/)|
|[NooDS](https://github.com/Hydr8gon/NooDS/)|<a href="https://github.com/Hydr8gon/NooDS/releases/tag/release"><img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="emoji">🐧🍎🟩</a>|No|

<!--- I don't really know, but it's Free now, and going Open Source soon
|[DraStic](https://www.drastic-ds.com/)|<a href="https://play.google.com/store/apps/details?id=com.dsemu.drastic">🟩</a>|No|
--->

<code><a href="https://mega.nz/#!KPwUHYCZ!mCzMRg3UN8UGJ2WKxAbCMaWVLUdAX0KCYHb0egCbrUk">nds</a></code>
<code><a download href="https://archive.org/download/dsi-nand-firmware-pwc/DSi%20NAND%2BFirmware%20PWC.zip">dsi</a></code>

<br>

### 3DS

|Emulator|LibRetro|
|---|---|
|[Citra ñ](https://github.com/PabloMK7/citra/)|No|
|[Lime3DS](https://github.com/Lime3DS/lime3ds-archive/)|No|
|[Mandarine](https://github.com/mandarine3ds/mandarine/)|No|
|[Panda3DS](https://panda3ds.com/)|No|

<!---
|[Azahar](https://azahar-emu.org/)|No|
|[Mikage](https://mikage.app/)|No|
|[Corgi3DS](https://github.com/PSI-Rockin/Corgi3DS/)|No|
--->

<code><a href="https://darthsternie.net/3ds-firmwares/">firmware</a></code>
<code><a href="https://mediafire.com/file/xf0i4pwijnsz3wo/3DS%20Shared%20Data.zip">files</a></code>
<code><a href="https://mega.nz/#!qUkWXISL!ivytO3ZgcBtUM1FqGR_0WKZBdrXDM_2_suJng4OJYno">boot</a></code>
<code><a href="https://github.com/K11MCH1/AdrenoToolsDrivers/">drivers</a></code>


<br>

## Sony

### PlayStation 1

|Emulator|LibRetro|
|---|---|
|[DuckStation](https://duckstation.org/)|[Yes](https://github.com/libretro/swanstation/)|
|[Mednafen](https://mednafen.github.io/)|[Yes](https://docs.libretro.com/library/beetle_psx_hw/)|
|[PCSX-Redux](https://pcsx-redux.consoledev.net/)|No|

<code><a href="https://mediafire.com/file/s11dvh2snfrmy29/PS1_BIOS.zip">bios</a></code>

<br>

### PlayStation 2

|Emulator|LibRetro|
|---|---|
|[PCSX2](https://pcsx2.net/)|[Yes](https://docs.libretro.com/library/pcsx2/)|
|[NetherSX2](https://github.com/Trixarian/NetherSX2-patch/)|No|
|[Play!](https://purei.org/)|[Yes](https://docs.libretro.com/library/play/)|

<code><a href="https://mega.nz/file/CZtzib7K#kQBwcq-zavhR7mLa-3OJPBvdWVmmmR1p7tgWhKjluE4">bios</a></code>

<br>

### PlayStation 3

|Emulator|LibRetro|
|---|---|
|[RPCS3](https://rpcs3.net/)|No|

<code><a href="https://playstation.com/support/hardware/ps3/system-software/">firmware</a></code>
<code><a href="https://darthsternie.net/ps3-firmwares/">backup</a></code>

<br>

### PSP

|Emulator|LibRetro|
|---|---|
|[PPSSPP](https://ppsspp.org/)|[Yes](https://docs.libretro.com/library/ppsspp/)|

<code><a href="https://darthsternie.net/psp-firmwares/">firmware</a></code>
<code><a href="https://mediafire.com/file/0k8654doty8wdl7/PSP+font.rar">fonts</a></code>

<br>

### PSVita

|Emulator|LibRetro|
|---|---|
|[Vita3K](https://vita3k.org/)|No|

<code><a href="https://playstation.com/support/hardware/psvita/system-software/">firmware</a></code>
<code><a href="https://darthsternie.net/ps-vita-firmwares/">backup</a></code>
<code><a href="https://github.com/K11MCH1/AdrenoToolsDrivers/">drivers</a></code>

<br>

## Sega

### Master System / Game Gear

|Emulator|LibRetro|
|---|---|
|[Genesis-Plus-GX](https://github.com/ekeeke/Genesis-Plus-GX/)|[Only](https://docs.libretro.com/library/genesis_plus_gx/)|
|[MAME](https://mamedev.org/)|[Yes<sup><abbr title="It's complicated">*</abbr></sup>](https://docs.libretro.com/guides/arcade-getting-started/)|
|[ares](https://ares-emu.net/)|<a href="https://ares-emu.net/download"><img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="emoji">🐧🍎</a>|No|

<code><a href="https://mega.nz/#!MAdyUKyI!14bp7kZYGaVYTwjFJeH8-wfxTYGS2Pkbj1lgNh_459Y">bios</a></code>

<br>

### Genesis / CD / 32X

|Emulator|LibRetro|
|---|---|
|[BlastEm](https://retrodev.com/blastem/)|[Yes](https://docs.libretro.com/library/blastem/)|
|[PicoDrive](https://github.com/libretro/picodrive/)|[Yes](https://docs.libretro.com/library/picodrive/)|
|[Genesis-Plus-GX](https://github.com/ekeeke/Genesis-Plus-GX/)|[Only](https://docs.libretro.com/library/genesis_plus_gx/)|
|[ares](https://ares-emu.net/)|<a href="https://ares-emu.net/download"><img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="emoji">🐧🍎</a>|No|

<code><a href="https://mega.nz/#!dMFgAS6B!btNyCsWZFSGE-NqFfJvo90vjXroeDoRdgQtGPwllODQ">genesis</a></code>
<code><a href="https://mediafire.com/file/wmmeiybqrml/Sega+Mega+CD+BIOS+Collection+v2.7z">cd</a></code>
<code><a href="https://mega.nz/#!YUNwhKwQ!uloygIwA7-8F30HFSfYIgxX2Z49_dIS3RJ29IIOLC9M">32x</a></code>

<br>

### Saturn

|Emulator|LibRetro|
|---|---|
|[Mednafen](https://mednafen.github.io/)|[Yes](https://docs.libretro.com/library/beetle_saturn/)|
|[Kronos](https://github.com/FCare/Kronos/)|[Yes](https://docs.libretro.com/library/kronos/)|

<code><a href="https://mediafire.com/file/1mne5inwr7rfws9/Saturn_BIOS_mednafen.zip">bios</a></code>

<br>

### Dreamcast

|Emulator|LibRetro|
|---|---|
|[Flycast](https://github.com/flyinghead/flycast/)|[Yes](https://docs.libretro.com/library/flycast/)|
|[redream](https://redream.io/)|No<sup><abbr title="Very old fork">*</abbr></sup>|

<code><a href="https://mediafire.com/file/ohpi7h3kh3ydli8/Dreamcast.zip">bios</a></code>

<br>

## Microsoft

### Xbox

|Emulator|LibRetro|
|---|---|
|[xemu](https://xemu.app/)|No|
|[Cxbx-Reloaded](https://cxbx-reloaded.co.uk/)|No|

<code><a href="https://mediafire.com/file/28zvvhqxjuoj4dp/Xbox_BIOS.zip">bios</a></code>
<code><a download href="https://github.com/xqemu/xqemu-hdd-image/releases/download/v1.0/xbox_hdd.qcow2.zip">hdd</a></code>

<br>

### Xbox 360

|Emulator|LibRetro|
|---|---|
|[xenia](https://xenia.jp/)|No|

<br>


## Atari

### 2600

|Emulator|LibRetro|
|---|---|
|[Stella](https://stella-emu.github.io/)|[Yes](https://docs.libretro.com/library/stella/)|
|[MAME](https://mamedev.org/)|[Yes<sup><abbr title="It's complicated">*</abbr></sup>](https://docs.libretro.com/guides/arcade-getting-started/)|

<br>

### 5200

|Emulator|LibRetro|
|---|---|
|[Atari800](https://atari800.github.io/)|[Yes](https://docs.libretro.com/library/atari800/)|
|[Altirra](https://virtualdub.org/altirra.html)|No|

<br>

### 7800

|Emulator|LibRetro|
|---|---|
|[MAME](https://mamedev.org/)|[Yes<sup><abbr title="It's complicated">*</abbr></sup>](https://docs.libretro.com/guides/arcade-getting-started/)|
|[A7800](http://7800.8bitdev.org/index.php/A7800_Emulator)|No|
|[ProSystem](https://gstanton.github.io/ProSystem1_3/)|[Yes](https://docs.libretro.com/library/prosystem/)|

<br>

### XEGS

|Emulator|LibRetro|
|---|---|
|[Atari800](https://atari800.github.io/)|[Yes](https://docs.libretro.com/library/atari800/)|
|[Altirra](https://virtualdub.org/altirra.html)|No|

<br>

### Jaguar

|Emulator|LibRetro|
|---|---|
|[BigPEmu](https://richwhitehouse.com/jaguar/index.php)|No|
|[Virtual Jaguar](https://icculus.org/virtualjaguar/)|[Yes](https://docs.libretro.com/library/virtual_jaguar/)|

<br>

### Lynx

|Emulator|LibRetro|
|---|---|
|[Mednafen](https://mednafen.github.io/)|[Yes](https://docs.libretro.com/library/beetle_lynx/)|
|[MAME](https://mamedev.org/)|[Yes<sup><abbr title="It's complicated">*</abbr></sup>](https://docs.libretro.com/guides/arcade-getting-started/)|

<code><a href="https://mediafire.com/file/kxni5x47psjrqqo/lynxboot.img/file">bios</a></code>

<br>
