# DNS

## General usage

|Name|IPv4|IPv6|DoT|DoH|
| ---|---|---|---|---|
|Google|8.8.8.8<br>8.8.4.4|2001:4860:4860::8888<br>2001:4860:4860::8844|dns.google|https<area>://dns.google/dns-query|
|Quad9|9.9.9.9<br>149.112.112.112|2620:fe::fe<br>2620:fe::9|dns.quad9.net|https<area>://dns.quad9.net/dns-query|
|AdGuard|94.140.14.14<br>94.140.15.15|2a10:50c0::ad1:ff<br>2a10:50c0::ad2:ff|dns.adguard.com|https<area>://dns.adguard.com/dns-query|
|CloudFlare|1.1.1.1<br>1.0.0.1|2606:4700:4700::1111<br>2606:4700:4700::1001|cloudflare-dns.com|https<area>://cloudflare-dns.com/dns-query|
|CloudFlare<br>Security|1.1.1.2<br>1.0.0.2|2606:4700:4700::1112<br>2606:4700:4700::1002|security.cloudflare-dns.com|https<area>://security.cloudflare-dns.com/dns-query|
|CloudFlare<br>Family|1.1.1.3<br>1.0.0.3|2606:4700:4700::1113<br>2606:4700:4700::1003|family.cloudflare-dns.com|https<area>://family.cloudflare-dns.com/dns-query|


### Bonus

These provide a service like [Pi-hole](https://pi-hole.net/) in the sense that you can control Content Filtering via DNS, but it's entirely off of your Home setup, and can be used anywhere.
It's not only Advertisements, but general Internet Protection, alongside caching, logging, and more.


[Rethink](https://rethinkdns.com/)

<!---
https://rethinkdns.com/configure?s=added#1:-J8AGP8Da_zZdQV-wP_iCmEwQmdCMAAI
https://rethinkdns.com/search?s=1:-J8AGP8Da_zZdQV-wP_iCmEwQmdCMAAI
--->

Personally I use these [DoT](https://1-7cpqagh7anv7zwlvav7mb77cbjqtaqthiiyaaca.max.rethinkdns.com) and [DoH](https://sky.rethinkdns.com/1:-J8AGP8Da_zZdQV-wP_iCmEwQmdCMAAI) settings, they block enough while not blocking stuff that I use.


[NextDNS](https://nextdns.io/)

NextDNS is more configurable than Rethink, allowing for example rewrites, [ipfs](https://ipfs.tech/), blacklist and whitelist, parental control, personal analytics, and improved security.

<i>Fun fact, you can use the system automatic proxy url for the pseudo DDNS.
Your OS will query the url, notice it's not a Proxy.PAC and fail silently, while still updating the IP.</i>


## Nintendo related

|Name|IPv4|Description|
|---|---|---|
|[WiiLink](https://wiilink24.com)|167.235.229.36|Use WiiConnect24 again|
|[Wimmfi](https://wiimmfi.de/patcher/dnspatch)|95.217.77.181|Play Wii online games again|
|[Kaeru WFC](https://kaeru.world/projects/wfc)|178.62.43.212|Play NDS online games again|
|[PokéCheats GTS](http://pokecheats.net/gts/)|199.199.163.34|Same as above, but you can [Generate mons](http://pokecheats.net/gts/upload-pokemon.php)|
|[90DNS](https://gitlab.com/a/90dns/-/blob/master/README.md)|207.246.121.77<br>163.172.141.219|Block Nintendo domains, prevents Bans|
|[str2hax](https://wiibrew.org/wiki/Str2hax)|3.143.163.250<br>173.201.71.14|Homebrew patch your Wii using the EULA|
