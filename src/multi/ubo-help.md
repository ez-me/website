# uBlock Origin Filter Help

<a href="./ubo-filters.txt">
<img src="../Media/m/ubo.webp" alt="🛡" loading="lazy" class="icon"> uBlock Origin Filter list</a>

This is my personal list, where I make my own filters.

I also have a list for enterprise networks, where there is [no fun allowed](./ubo-no-fun-allowed.txt).

And also, a [rules file](./ubo-rules.txt) that you have to manually add to the "My Rules" tab.

Oh, and just in case, [arkenfox](https://github.com/arkenfox/user.js/wiki/4.1-Extensions#-recommended) recommends [➗ Actually Legitimate URL Shortener Tool](https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt) and so do I.


## Invert blocking (anti-blacklist)

<code>@@||example.com/important$1p</code>

<br>

## Static network filtering

### Complete domain

<code>||blocked.tld^</code>

### Block a CSS section

<code>example.com##.ads</code>

### Block something based off of tags

<code>example.com##[href="/sponsor"]</code>

<code>example.com##.table [src="/ketchup']</code>

### Types options

<code>$script</code> <code>$image</code> <code>$css</code> <code>$media</code> <code>$1p</code> <code>$3p</code> <code>$font</code> <code>$frame</code> <code>$ping</code> <code>$inline-script</code> <code>$inline-font</code> <code>$match-case</code> <code>$xhr</code>

### Don’t allow a domain from another

<code>||blocked.tld^domain=example.com</code>

<h3> Don’t block something, redirect to an empty/noop file </h3>

<code>||example.org/folder/file$empty</code>

<code>||example.org/folder/file.js$script,redirect=noop.js</code>

<code>||example.org/folder/video.mp4$mp4</code>

<code>||example.org/folder/audio.mp3$media,redirect=noop-0.1s.mp3</code>

<code>||example.org/folder/image.png$image,redirect=2x2.png</code>

### Remove a URL parameter

<code>example.org$removeparam=TrackerID</code>

<br>

## Static extended filtering

### HTML filters

<i>use <abbr title="Control U">view-source</abbr>, not <abbr title="F12">inspect element</abbr></i>

<code>example.org##^.badstuff</code>

<code>example.com##^script:has-text(whatever)</code>

### Scriptlet injection

#### Cookie remover

<code>example.com##+js(cookie-remover)</code>

<code>example.com##+js(cookie-remover, cookie_name_regex)</code>
