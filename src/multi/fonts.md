# Fonts

<!---
||<a download href="">⬇️</a>|<a download href="">⬇️</a>|<a download href="">↙️</a>|<a download href="">⬇️</a>|<a download href="">↙️</a>|<a download href="">⬇️</a>|<a download href="">↙️</a>|
--->

## Latin

### Sans

|Typeface|Pack|V|VI|R|I|B|BI|
|---|---|---|---|---|---|---|---|
|[Atkinson Hyperlegible Pro](https://github.com/ez-me/atkinson-hyperlegible-pro/)|<a download href="https://github.com/ez-me/atkinson-hyperlegible-pro/releases/latest/download/AtkinsonHyperlegiblePro.TTF.zip">⬇️</a>|||<a download href="https://github.com/ez-me/atkinson-hyperlegible-pro/raw/main/fonts/ttf/AtkinsonHyperPro-Regular.ttf">⬇️</a>|<a download href="https://github.com/ez-me/atkinson-hyperlegible-pro/raw/main/fonts/ttf/AtkinsonHyperPro-Italic.ttf">↙️</a>|<a download href="https://github.com/ez-me/atkinson-hyperlegible-pro/raw/main/fonts/ttf/AtkinsonHyperPro-Bold.ttf">⬇️</a>|<a download href="https://github.com/ez-me/atkinson-hyperlegible-pro/raw/main/fonts/ttf/AtkinsonHyperPro-BoldItalic.ttf">↙️</a>|
|[Inter](https://rsms.me/inter/)|<a href="https://rsms.me/inter/download/">⬇️</a>|<a download href="https://gitlab.com/ez-me/settings/-/raw/main/fonts/InterKetsuban/InterKetsuban%20Roman.ttf?inline=false">⬇️</a>|<a download href="https://gitlab.com/ez-me/settings/-/raw/main/fonts/InterKetsuban/InterKetsuban%20Italic.ttf?inline=false">↙️</a>|||||
|[Noto Sans](https://fonts.google.com/noto/specimen/Noto+Sans)|<a download href="https://fonts.google.com/download?family=Noto%20Sans">⬇️</a>|<a download="NotoSans-Regular.ttf" href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSans/googlefonts/variable-ttf/NotoSans%5Bwdth,wght%5D.ttf">⬇️</a>|<a download="NotoSans-Italic.ttf" href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSans/googlefonts/variable-ttf/NotoSans-Italic%5Bwdth,wght%5D.ttf">↙️</a>|<a download href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSans/googlefonts/ttf/NotoSans-Regular.ttf">⬇️</a>|<a download href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSans/googlefonts/ttf/NotoSans-Italic.ttf">↙️</a>|<a download href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSans/googlefonts/ttf/NotoSans-Bold.ttf">⬇️</a>|<a download href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSans/googlefonts/ttf/NotoSans-BoldItalic.ttf">↙️</a>|
|[Source Sans 3](https://adobe-fonts.github.io/source-sans/)|<a download href="https://fonts.google.com/download?family=Source%20Sans%203">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-sans/raw/release/VAR/SourceSans3VF-Roman.ttf">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-sans/raw/release/VAR/SourceSans3VF-Italic.ttf">↙️</a>|<a download href="https://github.com/adobe-fonts/source-sans/raw/release/TTF/SourceSans3-Regular.ttf">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-sans/raw/release/TTF/SourceSans3-It.ttf">↙️</a>|<a download href="https://github.com/adobe-fonts/source-sans/raw/release/TTF/SourceSans3-Bold.ttf">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-sans/raw/release/TTF/SourceSans3-BoldIt.ttf">↙️</a>
|[Aptos](https://learn.microsoft.com/typography/font-list/aptos)|<a download href="https://www.microsoft.com/download/details.aspx?id=106087">⬇️</a>|||||||

<!---
 Should we add Apple's San Francisco?
 We could host the files ourselves...
--->



### Serif

|Typeface|Pack|V|VI|R|I|B|BI|
|---|---|---|---|---|---|---|---|
|[Times Newer Roman](https://timesnewerroman.com/)|<a download href="https://timesnewerroman.com/assets/TimesNewerRoman.zip">⬇️</a>|||||||
|[Spectral](https://github.com/productiontype/spectral)|<a download href="https://fonts.google.com/download?family=Spectral">⬇️</a>|||<a download href="https://github.com/productiontype/Spectral/raw/master/fonts/desktop/Spectral-Regular.ttf">⬇️</a>|<a download href="https://github.com/productiontype/Spectral/raw/master/fonts/desktop/Spectral-Italic.ttf">↙️</a>|<a download href="https://github.com/productiontype/Spectral/raw/master/fonts/desktop/Spectral-Bold.ttf">⬇️</a>|<a download href="https://github.com/productiontype/Spectral/raw/master/fonts/desktop/Spectral-BoldItalic.ttf">↙️</a>|
|[Noto Serif](https://fonts.google.com/noto/specimen/Noto+Serif)|<a download href="https://fonts.google.com/download?family=Noto%20Serif">⬇️</a>|<a download="NotoSerif-VF-Regular.ttf" href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSerif/googlefonts/variable-ttf/NotoSerif%5Bwdth%2Cwght%5D.ttf">⬇️</a>|<a download="NotoSerif-VF-Italic.ttf" href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSerif/googlefonts/variable-ttf/NotoSerif-Italic%5Bwdth%2Cwght%5D.ttf">↙️</a>|<a download href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSerif/googlefonts/ttf/NotoSerif-Regular.ttf">⬇️</a>|<a download href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSerif/googlefonts/ttf/NotoSerif-Italic.ttf">↙️</a>|<a download href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSerif/googlefonts/ttf/NotoSerif-Bold.ttf">⬇️</a>|<a download href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSerif/googlefonts/ttf/NotoSerif-BoldItalic.ttf">↙️</a>|
|[Source Serif 4](https://adobe-fonts.github.io/source-serif/)|<a download href="https://fonts.google.com/download?family=Source%20Serif%204">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-serif/raw/release/VAR/SourceSerif4Variable-Roman.ttf">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-serif/raw/release/VAR/SourceSerif4Variable-Italic.ttf">↙️</a>|<a download href="https://github.com/adobe-fonts/source-serif/raw/release/TTF/SourceSerif4-Regular.ttf">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-serif/raw/release/TTF/SourceSerif4-It.ttf">↙️</a>|<a download href="https://github.com/adobe-fonts/source-serif/raw/release/TTF/SourceSerif4-Bold.ttf">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-serif/raw/release/TTF/SourceSerif4-BoldIt.ttf">↙️</a>|

<!---
 Should we add Apple's New York?
 We could host the files ourselves...
--->


### Handwriting

|Typeface|Pack|V|VI|R|I|B|BI|
|---|---|---|---|---|---|---|---|
|[Shantell Sans](https://shantellsans.com/)|<a download href="https://fonts.google.com/download?family=Shantell%20Sans">⬇️</a>|<a download href="https://github.com/arrowtype/shantell-sans/raw/main/fonts/shantell_sans-for-googlefonts/ShantellSans%5BBNCE,INFM,SPAC,wght%5D.ttf">⬇️</a>|<a download href="https://github.com/arrowtype/shantell-sans/raw/main/fonts/shantell_sans-for-googlefonts/ShantellSans-Italic%5BBNCE,INFM,SPAC,wght%5D.ttf">↙️</a>|<a download href="https://github.com/arrowtype/shantell-sans/raw/main/fonts/Shantell%20Sans/Desktop/Static/TTF/Shantell_Sans-Normal-Regular.ttf">⬇️</a>|<a download href="https://github.com/arrowtype/shantell-sans/raw/main/fonts/Shantell%20Sans/Desktop/Static/TTF/Shantell_Sans-Normal-Regular_Italic.ttf">↙️</a>|<a download href="https://github.com/arrowtype/shantell-sans/raw/main/fonts/Shantell%20Sans/Desktop/Static/TTF/Shantell_Sans-Normal-Bold.ttf">⬇️</a>|<a download href="https://github.com/arrowtype/shantell-sans/raw/main/fonts/Shantell%20Sans/Desktop/Static/TTF/Shantell_Sans-Normal-Bold_Italic.ttf">↙️</a>|
|[Edu SA Beginner](https://fonts.googleblog.com/2022/08/the-handwriting-fonts-that-help.html)|<a download href="https://fonts.google.com/download?family=Edu%20SA%20Beginner">⬇️</a>|<a download href="https://github.com/MezMerrit/AU-School-Handwriting-Fonts/raw/main/SA-School-Fonts/fonts/variable/EduSABeginner%5Bwght%5D.ttf">⬇️</a>||<a download href="https://github.com/MezMerrit/AU-School-Handwriting-Fonts/raw/main/SA-School-Fonts/fonts/ttf/EduSABeginner-Regular.ttf">⬇️</a>||<a download href="https://github.com/MezMerrit/AU-School-Handwriting-Fonts/raw/main/SA-School-Fonts/fonts/ttf/EduSABeginner-Bold.ttf">⬇️</a>||



### Mono

|Typeface|Pack|V|VI|R|I|B|BI|
|---|---|---|---|---|---|---|---|
|[Courier Prime](https://quoteunquoteapps.com/courierprime/)|<a download href="https://quoteunquoteapps.com/courierprime/downloads/courier-prime.zip">⬇️</a>|||||||
|[Noto Mono](https://fonts.google.com/noto/specimen/Noto+Sans+Mono)|<a download href="https://fonts.google.com/download?family=Noto%20Sans%20Mono">⬇️</a>|<a download="NotoSansMono.ttf" href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSansMono/googlefonts/variable-ttf/NotoSansMono%5Bwdth%2Cwght%5D.ttf">⬇️</a>||<a download href="">⬇️</a>||<a download href="">⬇️</a>||
|[Source Code Pro](https://github.com/adobe-fonts/source-code-pro/)|<a download href="https://fonts.google.com/download?family=Source%20Code%20Pro">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-code-pro/raw/release/VF/SourceCodeVF-Upright.ttf">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-code-pro/raw/release/VF/SourceCodeVF-Italic.ttf">↙️</a>|<a download href="https://github.com/adobe-fonts/source-code-pro/raw/release/TTF/SourceCodePro-Regular.ttf">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-code-pro/raw/release/TTF/SourceCodePro-It.ttf">↙️</a>|<a download href="https://github.com/adobe-fonts/source-code-pro/raw/release/TTF/SourceCodePro-Bold.ttf">⬇️</a>|<a download href="https://github.com/adobe-fonts/source-code-pro/raw/release/TTF/SourceCodePro-BoldIt.ttf">↙️</a>|
|[Aptos](https://learn.microsoft.com/typography/font-list/aptos)|<a download href="https://www.microsoft.com/download/details.aspx?id=106087">⬇️</a>|||||||



### Code

|Typeface|en|en+nerd|em|em+nerd|
|---|---|---|---|---|
|[Fira Code](https://github.com/tonsky/FiraCode/)|||<a download href="https://gitlab.com/ez-me/settings/-/raw/main/fonts/FiraCode%20Lizzy/FiraCode%20Lizzy%20Variable.ttf?inline=false">⬇️</a>|<a download href="https://gitlab.com/ez-me/settings/-/raw/main/fonts/FiraCode%20Lizzy/FiraCode%20Lizzy%20Regular%20NF.ttf?inline=false">🤓</a>|
|[Iosevka](https://typeof.net/Iosevka/)|<a download href="https://gitlab.com/ez-me/settings/-/raw/main/fonts/Iosevka%20Ludovic/Iosevka%20Ludovic%20Regular.ttf?inline=false">⬇️</a>|<a download href="https://gitlab.com/ez-me/settings/-/raw/main/fonts/Iosevka%20Ludovic/Iosevka%20Ludovic%20Regular%20NF.ttf?inline=false">🤓</a>|<a download href="https://gitlab.com/ez-me/settings/-/raw/main/fonts/Iosevka%20Ludovic/Iosevka%20Ludovic%20Extended.ttf?inline=false">⬇️</a>|<a download href="https://gitlab.com/ez-me/settings/-/raw/main/fonts/Iosevka%20Ludovic/Iosevka%20Ludovic%20Extended%20NF.ttf?inline=false">🤓</a>|
|[Fixedsys Excelsior](https://github.com/kika/fixedsys/)|||<a download="FixedExcelsior.ttf" href="https://github.com/kika/fixedsys/releases/download/v3.02.9/FSEX302.ttf">⬇️</a>||
|[Monocraft](https://monocraft.idreesinc.com/)|||<a download href="https://github.com/IdreesInc/Monocraft/releases/latest/download/Monocraft.ttc">⬇️</a>|<a download href="https://github.com/IdreesInc/Monocraft/releases/latest/download/Monocraft-nerd-fonts-patched.ttc">🤓</a>|
|[Miracode](https://github.com/IdreesInc/Miracode)|||<a download href="https://github.com/IdreesInc/Miracode/releases/latest/download/Miracode.ttf">⬇️</a>||


## CJK

|Typeface|TTC|<abbr title="Simplified Chinese / Mandarin">SC</abbr>|<abbr title="Traditional Chinese (Hong Kong)">HK</abbr>|<abbr title="Traditional Chinese (Taiwan)">TW</abbr>|<abbr title="Japanese">JP</abbr>|<abbr title="Korean">KR</abbr>|
|---|---|---|---|---|---|---|
|[Noto CJK Sans](https://github.com/googlefonts/noto-cjk/tree/main/Sans#downloading-noto-sans-cjk)|<a download="NotoSansCJK.ttc" href="https://github.com/notofonts/noto-cjk/raw/main/Sans/Variable/OTC/NotoSansCJK-VF.ttf.ttc">⬇️</a>|<a download href="https://github.com/notofonts/noto-cjk/raw/main/Sans/Variable/TTF/NotoSansCJKsc-VF.ttf">⬇️</a>|<a download href="https://github.com/notofonts/noto-cjk/raw/main/Sans/Variable/TTF/NotoSansCJKhk-VF.ttf">⬇️</a>|<a download href="https://github.com/notofonts/noto-cjk/raw/main/Sans/Variable/TTF/NotoSansCJKtw-VF.ttf">⬇️</a>|<a download href="https://github.com/notofonts/noto-cjk/raw/main/Sans/Variable/TTF/NotoSansCJKjp-VF.ttf">⬇️</a>|<a download href="https://github.com/notofonts/noto-cjk/raw/main/Sans/Variable/TTF/NotoSansCJKkr-VF.ttf">⬇️</a>|
|[Noto CJK Serif](https://github.com/googlefonts/noto-cjk/tree/main/Serif#downloading-noto-serif-cjk)|<a download="NotoSerifCJK.ttc" href="https://github.com/notofonts/noto-cjk/raw/main/Serif/Variable/OTC/NotoSerifCJK-VF.ttf.ttc">⬇️</a>|<a download href="https://github.com/notofonts/noto-cjk/raw/main/Serif/Variable/TTF/NotoSerifCJKsc-VF.ttf">⬇️</a>|<a download href="https://github.com/notofonts/noto-cjk/raw/main/Serif/Variable/TTF/NotoSerifCJKhk-VF.ttf">⬇️</a>|<a download href="https://github.com/notofonts/noto-cjk/raw/main/Serif/Variable/TTF/NotoSerifCJKtw-VF.ttf">⬇️</a>|<a download href="https://github.com/notofonts/noto-cjk/raw/main/Serif/Variable/TTF/NotoSerifCJKjp-VF.ttf">⬇️</a>|<a download href="https://github.com/notofonts/noto-cjk/raw/main/Serif/Variable/TTF/NotoSerifCJKkr-VF.ttf">⬇️</a>|


<!---
 Should we add Jigmo? It's Pan-CJK
[Jigmo](https://kamichikoichi.github.io/jigmo/)|
https://kamichikoichi.github.io/jigmo/Jigmo-20230816.zip

 Should we add I.Ming? It's for TC (General)
[I.Ming]( https://github.com/ichitenfont/I.Ming/)
https://github.com/ichitenfont/I.Ming/raw/master/8.10/I.Ming-8.10.ttf

 Should we add BabelStone Han?
https://www.babelstone.co.uk/Fonts/Han.html
https://www.babelstone.co.uk/Fonts/Download/BabelStoneHan.zip

 Should we add Code2000?
https://www.code2001.com/
https://code2001.com/CODE2000.ZIP
--->


## Emoji, Symbols

[Twemoji](https://github.com/jdecked/twemoji/)
<a download="Twemoji.ttf" href="https://artefacts.whynothugo.nl/twemoji.ttf/">⬇️</a>

[Noto Color Emoji](https://fonts.google.com/noto/specimen/Noto+Color+Emoji)
<a download="NotoColorEmoji.ttf" href="https://github.com/googlefonts/noto-emoji/raw/main/fonts/NotoColorEmoji_WindowsCompatible.ttf">⬇️</a>
<a download="NotoColorEmoji.ttf" href="https://github.com/googlefonts/noto-emoji/raw/main/fonts/Noto-COLRv1.ttf">⬇️</a>

[Noto Emoji](https://fonts.google.com/noto/specimen/Noto+Emoji)
<a download href="https://fonts.google.com/download?family=Noto%20Emoji">⬇️</a>

[Noto Sans Symbols 2](https://fonts.google.com/noto/specimen/Noto+Sans+Symbols+2)
<a download="NotoSansSymbols2.ttf" href="https://github.com/notofonts/notofonts.github.io/raw/main/fonts/NotoSansSymbols2/googlefonts/ttf/NotoSansSymbols2-Regular.ttf">⬇️</a>

[Symbola](https://dn-works.com/ufas/)
<a download href="../fonts/Symbola.ttf">⬇️</a>


## Fantasy

[Nasin nanpa](https://github.com/ETBCOR/nasin-nanpa)
<a download href="https://github.com/ETBCOR/nasin-nanpa/raw/main/versions/nasin-nanpa.otf">⬇️</a>

