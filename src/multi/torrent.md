# Torrent

## Searching

<a href="https://torrents-csv.com/">
 Torrents.CSV</a>
<br><br>

<a href="https://btdig.com/">
 BTDigg</a>
<br><br>

<a href="https://academictorrents.com/">
 Academic Torrents</a>
<br><br>


## Download

### Local

<a href="https://github.com/c0re100/qBittorrent-Enhanced-Edition/">
 qBittorrent Enhanced</a>
<a href="https://github.com/c0re100/qBittorrent-Enhanced-Edition/releases/latest/">⬇️</a>
<br><br>

<!---
<a href="https://qbittorrent.org/">
 qBittorrent</a>
<a href="https://qbittorrent.org/download/">⬇️</a>
<br><br>
--->

<a href="https://deluge-torrent.org/">
 Deluge</a>
<a href="https://dev.deluge-torrent.org/wiki/Download/">⬇️</a>
<br><br>

<a href="https://motrix.app/">
 Motrix</a>
<a href="https://github.com/agalwood/Motrix/releases/latest/">⬇️</a>
<br><br>

<a href="https://gitlab.com/proninyaroslav/libretorrent/">
 LibreTorrent</a>
<a href="https://play.google.com/store/apps/details?id=org.proninyaroslav.libretorrent/"><img src="../Media/a/playstore.webp" alt="🔷" loading="lazy" class="emoji"></a>
<a href="https://f-droid.org/packages/org.proninyaroslav.libretorrent/"><img src="../Media/a/fdroid.webp" alt="🟩" loading="lazy" class="emoji"></a>
<br>


### Web

<a href="https://instant.io/">
 Instant.io</a>
<br><br>

<a href="https://btorrent.xyz/">
 βTorrent</a>
<br><br>

<a href="https://seedr.cc/">
 Seedr</a>
<br>


## Trackers

<a href="https://trackerslist.com/">
 XIU2/TrackersListCollection</a>
<a href="https://cf.trackerslist.com/all.txt">📎</a>
<br><br>

<!---
<a href="https://ngosang.github.io/trackerslist/">
 ngosang/trackerlists</a>
<a href="https://ngosang.github.io/trackerslist/trackers_all.txt">📎</a>
<br><br>

<a href="https://newtrackon.com/">
 newTrackon</a>
<a href="https://newtrackon.com/list">📎</a>
<br>
--->
