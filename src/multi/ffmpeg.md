# FFMPEG Help

Want a better experience to what the [official FFmpeg Filters Documentation](https://ffmpeg.org/ffmpeg-filters.html) shows?

Use [FFmpeg Filters Docs](https://ayosec.github.io/ffmpeg-filters-docs/)!


## General options

### Hardware acceleration

```bash
ffmpeg -hwaccel "auto"
```

|CPU|[VAAPI](https://trac.ffmpeg.org/wiki/Hardware/VAAPI)|[NVIDIA](https://trac.ffmpeg.org/wiki/HWAccelIntro#CUDANVENCNVDEC)|[AMD](https://trac.ffmpeg.org/wiki/HWAccelIntro#AMDUVDVCE)|[Intel](https://trac.ffmpeg.org/wiki/Hardware/QuickSync)|
|---|---|---|---|---|
|libsvtav1<br>libdav1d|av1_vaapi|av1_nvenc<br>av1_cuvid|av1_amf||
|libx264<br>h264|h264_vaapi|h264_nvenc<br>h264_cuvid|h264_amf|h264_qsv|
|libx265<br>hevc|hevc_vaapi|hevc_nvenc<br>hevc_cuvid|hevc_amf|hevc_qsv|
|libvpx|vp8_vaapi||||
|libvpx-vp9|vp9_vaapi|||vp9_qsv|

### Less verbose

```bash
ffmpeg -hide_banner -v "error" -stats
```

## Video Filters

### Constant FPS

Duplicate/Drop frames
```bash
-vf "fps=60"
```

(linearly) Interpolate frames
```bash
-vf "framerate=60"
```

### Fast Denoise

```bash
-vf "hqdn3d=1:1:4:4"
```

<!---
Thanks https://mattgadient.com/in-depth-look-at-de-noising-in-handbrake-with-imagevideo-examples/
--->

#### Slow Denoise

```bash
-vf "nlmeans=2:7:5:3:3"
```

<!---
Thanks https://forum.videohelp.com/threads/398880-Optimising-Performance-of-NLMeans-in-FFmpeg
--->

<!---
Check nlmeans_opencl
--->

```bash
-hwaccel vulkan -i $input -vf "format=yuv444p,hwupload,nlmeans_vulkan=2:7:3,hwdownload,format=yuv444p" $output
```

### Deinterlace

```bash
-vf "yadif"
```

<!---
Check yadif_cuda
--->

### Resize

```bash
-vf "scale=1920x-1"
```

<i>Using -1 as a size preserves the aspect ratio.</i>

<!---
Check scale_cuda
--->

## Audio Filters

### 5.1 to Stereo

```bash
-af "volume=1.660156,pan=stereo|FL=0.5*FC+0.707*FL+0.707*BL+0.707*SL+0.5*LFE|FR=0.5*FC+0.707*FR+0.707*BR+0.707*SR+0.5*LFE" 
```

<!---
Thanks https://superuser.com/a/1375875 and https://superuser.com/a/1663861
--->

### Loudness Normalization

```bash
-af "loudnorm" -pass 1 -f null /dev/null 
&&
-af "loudnorm" -ar 48000 -pass 2 $output 
```

## Video double pass

```bash
-c:v libx265 -crf 25 -pass 1 -f null /dev/null 
&&
-c:v libx265 -crf 25 -pass 2 $output 
```

```bash
-c:v libvpx-vp9 -row-mt 1 -b:v 0 -crf 25 -pass 1 -f null /dev/null 
&&
-c:v libvpx-vp9 -row-mt 1 -b:v 0 -crf 25 -pass 2 $output 
```

<!---
Not all codecs allow -crf double pass
--->

## Add thumbnails


### MKV

```bash
-i video.mkv -attach cover.jpg -metadata:s:t:0 mimetype=image/jpeg -c copy out.mkv
```

<i>Image has to fit in a 600px square</i>

<!---
Thanks https://video.stackexchange.com/a/35957
--->

### MP4

```bash
-i video.mp4 -i cover.jpg -map 1 -map 0 -c copy -disposition:0 attached_pic out.mp4
```

<!---
Thanks https://stackoverflow.com/a/54719926
--->

### MP3

```bash
-i audio.mp3 -i cover.jpg -map 0:0 -map 1:0 -c copy -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" out.mp3
```

<!---
Thanks https://stackoverflow.com/a/18718265
--->

### Opus

```bash
-i audio.opus -i cover.jpg -c copy -disposition attached_pic out.opus
```

<i>Image has to be less than around 750kB</i>

<!---
Thanks https://unix.stackexchange.com/a/774761
--->

