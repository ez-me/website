# MkvToolNix Help

## Experimental settings

```bash
MTX_ENGAGE="write_headers_twice,space_after_chapters"
```

## MkvMerge

```bash
mkvmerge --title $title --default-language en -o $output $files
```

## MkvPropEdit

```bash
mkvpropedit --edit track:a1 --set name=English $output
```
```bash
mkvpropedit --edit track:s1 --set name=English $output
```
```bash
mkvpropedit --edit track:v1 --set name=$title $output
```
