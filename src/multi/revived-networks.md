# Revived Networks

## Nintendo

[Wimmfi](https://wiimmfi.de/)

Works for the Wii and DS.

[Pretendo Network](https://pretendo.network/)

Works for the Wii<sup>U</sup> and (new) 3DS.
<br>

## Xbox

[Insignia](https://insignia.live/)

Works for the OG Xbox.
<br>

## PC

[333networks](https://333networks.com/)

[OpenSpy](http://beta.openspy.net/)

Both work to revive GameSpy for PC, and OpenSpy also works for some PS2 games!

