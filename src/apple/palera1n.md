# Install palera1n

## Preparation

First, we have to install <code>git</code> and <code>python</code>.
<br><i>If you have another package manager, change the command accordingly as shown [Here](https://wiki.archlinux.org/title/Pacman/Rosetta).</i>

<pre>
</pre>

```bash
sudo apt install git python3 python3-pip
```

## usbmuxd

We want to handle the daemon <code>usbmuxd</code> manually, so we have to stop the service and initiate it ourselves.

<pre>
</pre>

```bash
sudo systemctl stop usbmuxd
```

```bash
sudo usbmuxd -f -p
```

Leave this terminal open, and open yet another one to continue.
<br>

## git

Now it's time to download palera1n, it's important to note however that we need to use an specific method, due to the quirkiness of handling source control.

<pre>
</pre>

```bash
git clone --recursive --depth=1 --shallow-submodules https://github.com/palera1n/palera1n.git
```

```bash
cd palera1n
```

## Installing

Finally it's time to install palera1n on your device, first make sure it doesn't have a passcode or Touch/Face ID (you might need to "restore" it to factory settings).

Now, we just run the main command, replacing that variable with the actual iOS version (like 15.7.3):
<br>

```bash
sudo ./palera1n.sh --tweaks $iOS_ver --semi-tethered
```

It will probably ask to install a few dependencies -like pyimg4-, just hit Enter and let it continue.

Eventually it will ask you to get your device in DFU mode, helpfully providing instructions (depends on device).

### problems?

If it doesn't work the first time, or it gets stuck in a step, try repeating the usbmuxd step, and running the script again. Changing the USB port, turning the lighting cable, using an entirely different cable, another Linux distro, another Computer (with Intel CPU), etc.

## Finishing

After it's done, you have to get into DFU mode once again, the device will boot and you'll see the palera1n loader app in your home screen, open it and click install, which will eventually install Sileo.
