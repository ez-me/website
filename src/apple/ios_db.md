# iOS Databases

[iOS Ref](https://iosref.com/ios)
Shows in a simple table which devices are able to access which iOS versions.

[AppleDB](https://appledb.dev/)
In Depth website to have the iOS and firmware versions of any device, alongside jailbreak capabilities.

[Can I Jailbreak?](https://canijailbreak.com/)
Dedicated to knowing about JailBreak capabilities.

