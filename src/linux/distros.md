# Distros

<i>Please use mirrors that are closer to you, or [torrent](../multi/torrent).</i>


## Alpine

<a href="https://alpinelinux.org/">
<img src="../Media/l/alpine.webp" alt="🏔️" loading="lazy" class="icon"> Alpine Linux</a>
<br><br>

<i>Uses [musl](https://musl.libc.org/) instead of [glibc](https://www.gnu.org/software/libc/).</i>


## Android

<a href="https://blissos.org/">
<img src="../Media/l/blissos.webp" alt="🪷" loading="lazy" class="icon"> Bliss OS</a>


## Arch

### General use

<a href="https://archlinux.org/">
<img src="../Media/l/arch.webp" alt="🔼" loading="lazy" class="icon"> Arch Linux</a>

<i>Hard to install? <a href="./install-arch-efi.html">check my guide</a>.</i>

<pre>
</pre>

<a href="https://garudalinux.org/">
<img src="../Media/l/garuda.webp" alt="🦅" loading="lazy" class="icon"> Garuda Linux</a>
<br><br>

<a href="https://artixlinux.org/">
<img src="../Media/l/artix.webp" alt="🔼" loading="lazy" class="icon"> Artix Linux</a>
<br><br>

<i>Doesn't use <code>systemd</code>, but rather <code>OpenRC</code>, <code>Runit</code>, <code>s6</code>, or <code>dinit</code>.</i>
<br><br>


### Specialized

<a href="https://blackarch.org/">
<img src="../Media/l/blackarch.webp" alt="🗡️" loading="lazy" class="icon"> BlackArch</a>
<br><br>

<a href="https://maid.binbash.rocks/">
<img src="../Media/l/maid.webp" alt="🧹" loading="lazy" class="icon"> [m]anage your [A]ndro[id]</a>
<br><br>

<a href="https://system-rescue.org/">
<img src="../Media/l/system-rescue.webp" alt="📀" loading="lazy" class="icon"> System Rescue</a>
<br><br>


## Debian

### Regular Use

<a href="https://debian.org/">
<img src="../Media/l/debian.webp" alt="🔴" loading="lazy" class="icon"> Debian</a>
<br><br>

<a href="https://spirallinux.github.io/">
<img src="../Media/l/spirallinux.webp" alt="🔴" loading="lazy" class="icon"> SpiralLinux</a>
<br><br>

<a href="https://linuxmint.com/">
<img src="../Media/l/mint.webp" alt="🟩" loading="lazy" class="icon"> Linux Mint</a>
<br><br>

<a href="https://vanillaos.org/">
<img src="../Media/l/vanillaos.webp" alt="🏵️" loading="lazy" class="icon"> Vanilla OS</a>
<br><br>

### Privacy / Hacking / Security

<a href="https://kali.org/">
<img src="../Media/l/kali.webp" alt="🐲" loading="lazy" class="icon"> Kali Linux</a>
<br><br>

<a href="https://parrotsec.org/">
<img src="../Media/l/parrot.webp" alt="🦜" loading="lazy" class="icon"> Parrot OS</a>
<br><br>

<a href="https://tails.boum.org/">
<img src="../Media/l/tails.webp" alt="🔮" loading="lazy" class="icon"> Tails</a>

<i>Tails is strictly a LiveISO system, no installation is needed or indeed possible.</i>
<br><br>


## Red Hat

### Official

<a href="https://centos.org/">
<img src="../Media/l/centos.webp" alt="🔶" loading="lazy" class="icon"> CentOS</a>
<br><br>

<a href="https://getfedora.org/">
<img src="../Media/l/fedora.webp" alt="🔵" loading="lazy" class="icon"> Fedora</a>
<br><br>

### Gaming

<a href="https://nobaraproject.org/">
<img src="../Media/l/nobara.webp" alt="🇳" loading="lazy" class="icon"> Nobara</a>
<br><br>


## OpenSUSE

<a href="https://opensuse.org/">
<img src="../Media/l/opensuse.webp" alt="🦎" loading="lazy" class="icon"> OpenSUSE</a>
<br><br>

<a href="https://geckolinux.github.io/">
<img src="../Media/l/geckolinux.webp" alt="🦎" loading="lazy" class="icon"> GeckoLinux</a>
<br><br>


## Puppy

<a href="https://puppylinux-woof-ce.github.io/">
<img src="../Media/l/puppy.webp" alt="🐶" loading="lazy" class="icon"> Puppy Linux</a>

<i>Puppy isn't one single distro, but a RAM-only based on binaries of other distros.</i>

<pre>
</pre>

<a href="https://vanilla-dpup.github.io/">
<img src="../Media/l/vanilla-dpup.webp" alt="🐶" loading="lazy" class="icon"> Vanilla Dpup</a>
<br><br>
