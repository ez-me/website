# Analyze Slow Boot times

## System information

```bash
inxi -Frxxxz
```

<i>Displays a lot of System Information</i>

<br>

## Programs and Services

```bash
systemd-analyze
```

<i>Simple boot times info</i>
<br>

```bash
systemd-analyze blame | head
```

<i>Shows the 10 most slow services at boot</i>
<br>

```bash
systemd-analyze critical-chain
```

<i>Shows in red which programs were the slowest, in a chain of loading priorities.</i>
<br>

## Partitions

```bash
cat /etc/fstab
```

<i>Displays how your partitions are mounted</i>
<br>

```bash
lsblk -o NAME,LABEL,UUID,MOUNTPOINT
```

<i>Lists what is actually mounted in the system, should be equal to the above</i>
<br>

```bash
cat /etc/initramfs-tools/conf.d/resume
```

<i>Should show the SWAP partition’s UUID</i>
