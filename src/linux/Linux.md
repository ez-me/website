# Linux

- [Distros](./distros.md)
- [Analyze SlowBoot](./slowboot.md)
- Tutorials
    - [Install Arch on EFI](./install-arch-efi.md)
    - [Use the AUR](./use-the-aur.md)
