# Use the ArchLinux User Repository

## 1. AUR Helper

### paru

```bash
sudo pacman -S --needed base-devel git

git clone https://aur.archlinux.org/paru.git

cd paru

makepkg -si
```

#### Config

<i>This config makes it so that by default it doesn't always asks you to edit PKGCONFIG or show you how the source files were modified.</i>
<br>

|#|<code>nano ~/.config/paru/paru.conf</code>|
|---|---|
|1|<code>[options]</code>|
|2|<code>SkipReview</code>|

<br>

#### Usage

Now you can just use <code>paru</code> as opposed to <code>pacman</code>, and it will install AUR packages automatically.

eg.
```bash
paru -S 7-zip-full
```
<i>Keep in mind paru automatically calls <code>sudo</code>, so you don't require to use it.</i>

## 2. Custom Repo

### Chaotic-AUR

First, install the primary key

<pre>
</pre>

```bash
sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com

sudo pacman-key --lsign-key FBA220DFC880C036

sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
```

Secondly, we need to activate the multilib repository

|#|<code>sudo nano /etc/pacman.conf</code>|
|---|---|
|93|<code>[multilib]</code>|
|94|<code>Include = /etc/pacman.d/mirrorlist</code>|

<pre>
</pre>

Then, add these lines to the end of the pacman config file
<br>

|#|<code>sudo nano /etc/pacman.conf</code>|
|---|---|
|102|<code>[chaotic-aur]</code>|
|103|<code>Include = /etc/pacman.d/chaotic-mirrorlist</code>|

<pre>
</pre>

Finally, run a regular upgrade to have it properly setup.
<br>

```bash
sudo pacman -Syu
```

#### Usage

Now the AUR is available with regular <code>pacman</code>, so you can keep using it as normal.

eg.
```bash
sudo pacman -S 7-zip-full
```

One advantage of using this repository, is that it will just provide packages pre-compiled, so you get everything in one go, but it has less packages due to space limitations.

