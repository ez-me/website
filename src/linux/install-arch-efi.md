# Install Arch Linux (EFI)

After [Downloading](https://archlinux.org/download/) the ISO and writing it on a DVD or Pendrive (or using [Ventoy](https://ventoy.net/)), just boot into the installation media and follow these commands.

If you are reading this with [elinks](http://elinks.or.cz/), [links](http://links.twibright.com/), [lynx](https://lynx.invisible-island.net/), or [w3m](https://salsa.debian.org/debian/w3m)...
Hi! and thanks for your preference :)

## Use fast and pretty settings

```bash
reflector -c cl --sort score --save /etc/pacman.d/mirrorlist
```

<i>Change <code>cl</code> for your [ccTLD](https://en.wikipedia.org/wiki/Country_code_top-level_domain) of choice</i>

|#|<code>nano /etc/pacman.conf</code>|
|---|---|
|33|<code>Color</code>|
|37|<code>ParallelDownloads = 5</code>|

<pre>
</pre>

```bash
pacman -Syy
```

## Prepare the Storage Media

<!---
TO DO: explain about not needing swap if you have a lot of physical ram
--->

<i>tip: if you have troubles with the file table, use this command to overwrite it</i>
<br>

```bash
dd if=/dev/zero of=/dev/sda count=100 status=progress
```

```bash
cfdisk /dev/sda
```

<i>use gpt</i>

|Index|Size|Mount|
|---|---|---|
| [1] | 1GB | /boot |
| [2] | big | / |
| [3] | little | swap |

<br>

### Format the filesystems

```bash
mkfs.fat -F32 -n "ESP" /dev/sda1
```

```bash
mkfs.btrfs -L "Root" -f /dev/sda2
```

```bash
mkswap -L "swap" /dev/sda3
```

### Mount stuff
```bash
swapon /dev/sda3
```

```bash
mount /dev/sda2 /mnt
```

```bash
mkdir /mnt/boot
```

```bash
mount /dev/sda1 /mnt/boot
```

```bash
genfstab -U /mnt > /mnt/etc/fstab
```

### Compression

In the fstab file, you can add the following options to enable compression, here is an example:

|#|<code>nano /mnt/etc/fstab</code>|
|---|---|
|1|# /dev/sda2 LABEL=Root|
|2|UUID=a-b-c-d-e / btrfs <b>compress=zstd</b>,rw,space_cache=v2 0 0|

<pre>
</pre>

Also, add lazytime to the options for better performance.

<pre>
</pre>

|#|<code>nano /mnt/etc/fstab</code>|
|---|---|
|1|# /dev/sda2 LABEL=Root|
|2|UUID=a-b-c-d-e / btrfs <b>lazytime</b>,compress=zstd,rw,space_cache=v2 0 0|

<pre>
</pre>

Now remount the drive in order for the options to apply.

<pre>
</pre>

```bash
umount -R /mnt
```

```bash
mount -o compress=zstd /dev/sda2 /mnt
```

```bash
mount /dev/sda1 /mnt/boot
```



## Install the system

```bash
pacstrap /mnt base base-devel
```

### Install the kernel

```bash
pacstrap /mnt linux-zen linux-firmware dkms linux-zen-headers
```

### Install filesystem programs

```bash
pacstrap /mnt btrfs-progs dosfstools mtools refind efibootmgr
```

### Install network programs

```bash
pacstrap /mnt ntp iptables-nft networkmanager dnsmasq reflector
```

### Install audio programs

```bash
pacstrap /mnt pipewire-alsa wireplumber qpwgraph
```

## Log into the new system

```bash
arch-chroot /mnt
```

----

### Configure Locale

<i>Goes without saying, but if you speak another language, change these two commands.</i>

<pre>
</pre>

```bash
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
```

```bash
echo "LANG=en_US.UTF-8" > /etc/locale.conf
```

```bash
locale-gen
```

### Configure time

<i>Same with the time, if you live in another time zone, use that instead.</i>

<pre>
</pre>

```bash
ln -s /usr/share/zoneinfo/America/Santiago /etc/localtime
```

```bash
hwclock --systohc --utc
```

```bash
systemctl enable ntpd
```

## Configure Names and Network

<i>You can use any hostname, this is just an example.</i>

<pre>
</pre>

```bash
echo awa > /etc/hostname
```

|#|<code>nano /etc/hosts</code>|
|---|---|
|1|<code>127.0.0.1 localhost</code>|
|2|<code>::1 locahost</code>|
|3|<code>127.0.1.1 awa</code>|

<br>

```bash
systemctl enable NetworkManager
```

|#|<code>nano /etc/NetworkManager/conf.d/dns.conf</code>|
|---|---|
|1|<code>[main]</code>|
|2|<code>dns=dnsmasq</code>|

<i>[dnsmasq settings](https://gitlab.com/ez-me/settings/-/raw/main/linux/config/dnsmasq.conf) are now in <code>/etc/NetworkManager/dnsmasq.d/</code> due to NetworkManager.</i>

<br>

### Configure Users

#### Root

```bash
passwd
```

#### Regular user

<i>You probably want to use your own nickname for the username.</i>

<pre>
</pre>

```bash
useradd -m -g users -G wheel -s /bin/bash uwu
```

```bash
passwd uwu
```

|#|<code>EDITOR=rnano visudo</code>|
|---|---|
|85|<code>%wheel ALL=(ALL:ALL) ALL</code>|

<br>

### Use fast and pretty settings

```bash
reflector -c cl --sort score --save /etc/pacman.d/mirrorlist
```

<i>Change <code>cl</code> for your [ccTLD](https://en.wikipedia.org/wiki/Country_code_top-level_domain) of choice</i>

|#|<code>nano /etc/pacman.conf</code>|
|---|---|
|33|<code>Color</code>|
|37|<code>ParallelDownloads = 5</code>|

<pre>
</pre>

```bash
pacman -Syy
```

## Install BootLoader

```bash
refind-install --usedefault /dev/sda1 --alldrivers
```

```bash
mkrlconf
```

```bash
cd /boot
```

### Configure rEFInd

|#|<code>nano refind_linux.conf</code>|
|---|---|
|1|"Minimal" "rw root=/dev/sda2"|

<br>

<i>This is a very small setup, a more complete one looks like this:</i>

<pre>
</pre>

|#|<code>nano refind_linux.conf</code>|
|---|---|
|1|"micro Code" "rw root=/dev/sda2 initrd=amd-ucode.img initrd=initramfs-linux-zen.img"|
|2|"Minimal" "rw root=/dev/sda2"|
|3|"Regular" "rw root=/dev/sda2 initrd=initramfs-linux-zen.img"|
|4|"Fallback" "rw root=/dev/sda2 initrd=initramfs-linux-zen-fallback.img"|

<i>These files assume you installed <code>linux-zen</code> and are using an AMD CPU, change those accordingly.</i>

However, keep in mind you will probably want to use PARTUUID in order to use more storage devices at boot.

<pre>
</pre>

```bash
lsblk -o LABEL,PARTUUID | grep Root
```

and change the file as needed

<pre>
</pre>

|#|<code>nano refind_linux.conf</code>|
|---|---|
|1|"Minimal" "rw root=PARTUUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"|

<pre>
</pre>

### Exit the system

```bash
exit
```

----

### Unmount everything and reboot

```bash
sync
```

```bash
umount -R /mnt
```

```bash
reboot
```

remember to remove your installation media!
