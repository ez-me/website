# Bash if statements

# Test

You can check if the condition is true with the command <code>test</code>, here are tables of possible tests.

<pre>
</pre>

|Operator|Description|
|---|---|
|! EXP|EXP is false|

<pre>
</pre>

|Operator|Description|
|---|---|
|-n STR|Length of STR is non-zero|
|-z STR|Length of STR is zero (empty)|
|STR1 = STR2|STR1 is equal to STR2|
|STR1 != STR2|STR1 is different to STR2|

<pre>
</pre>

|Operator|Description|
|---|---|
|NUM1 -eq NUM2|NUM1 is equal to NUM2|
|NUM1 -ne NUM2|NUM1 is equal to NUM2|
|NUM1 -gt NUM2|NUM1 is greater than NUM2|
|NUM1 -ge NUM2|NUM1 is greater than or equal to NUM2|
|NUM1 -lt NUM2|NUM1 is less than NUM2|
|NUM1 -le NUM2|NUM1 is less than or equal to NUM2|

<pre>
</pre>

|Operator|Description|
|---|---|
|-e FILE|FILE Exists|
|-s FILE|FILE Exists, and is not empty|
|-f FILE|FILE Exists, and it's a regular file|
|-d FILE|FILE Exists, and is a directory|
|-r FILE|FILE Exists, and it can be Read (r--)|
|-w FILE|FILE Exists, and it can be Written (-w-)|
|-x FILE|FILE Exists, and it can be eXecuted (--x)|

<pre>
</pre>

## Basic

```bash
if [ 2 -eq 2 ]
then
   echo "yes"
fi
```

## Else

```bash
if [ "2" = 2 ]
then
   echo "yes"
else
   echo "no"
fi
```

## Else If

```bash
if [ 2 -ge 2 ]
then
   echo "yes"
elif [ 2 -lt 2 ]
then
   echo "no"
fi
```

## Boolean Logic

```bash
if [ 2 -eq 2 ] && [ 2 -ge 2 ]
then
   echo "yes"
fi
```

```bash
if [ "2" = 2 ] || [ 2 -lt 2 ]
then
   echo "yes"
fi
```

## Case

```bash
case $1 in
   2)
   echo "2"
   ;;
   a)
   echo "a"
   ;;
   *)
   echo "error"
   ;;
esac
```
