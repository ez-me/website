# Integrated Tools

<i>If the name has a <img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Shield, it requires Admin Privileges.</i>

----

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Add Hardware
```cmd
hdwwiz.exe
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Add/Remove Windows Features
```cmd
OptionalFeatures.exe
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Boot options
```cmd
msconfig.exe
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Check memory problems
```cmd
MdSched.exe
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Clean Up the WinSxS Folder
```cmd
Dism.exe /online /Cleanup-Image /StartComponentCleanup /ResetBase
```

Clean thumbnail cache
```cmd
ie4uinit.exe
```

Command Console
```cmd
cmd.exe
```

Control Panel
```cmd
control.exe
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Create Self Extracting file
```cmd
iexpress.exe
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Defragment and Optimize
```cmd
dfrgui.exe
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Defragment Drive
```powershell
Optimize-Volume -Verbose -Defrag -DriveLetter DRIVE_LETTER
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Device Manager
```cmd
devmgmt.msc
```

DirectX information
```cmd
dxdiag.exe
```

Download File
```cmd
bitsadmin.exe /transfer myDownloadJob URL PATH\TO\FILE
```

<!-- e -->

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Free Storage Space
```cmd
cleanmgr.exe
```

Gamepads
```cmd
joy.cpl
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Group Policy Editor
```cmd
gpedit.msc
```

<!-- h i j k -->

Lock User
```cmd
rundll32.exe user32.dll,LockWorkStation
```

<!-- m n -->

On Screen Keyboard
```cmd
osk.exe
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Optimize SSD
```powershell
Optimize-Volume -Verbose -ReTrim -DriveLetter DRIVE_LETTER
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Performance Options
```cmd
SystemPropertiesPerformance.exe
```

<!-- q -->

Run Background Tasks
```cmd
rundll32.exe advapi32.dll,ProcessIdleTasks
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Services
```cmd
services.msc
```

Sound Settings
```cmd
mmsys.cpl
```

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Storage Manager
```cmd
diskmgmt.msc
```

System Information
```cmd
msinfo32.exe
```

System Properties
```cmd
sysdm.cpl
```

<!-- t -->

<img src="../Media/w/admin.webp" alt="🛡" loading="lazy" class="emoji">Update the Clock
```cmd
w32tm.exe /config /update /manualpeerlist:"pool.ntp.org" /syncfromflags:MANUAL /reliable:YES
```

<!-- v -->

Windows Version
```cmd
winver.exe
```

<!-- x y z -->

