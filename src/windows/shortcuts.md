# Shortcuts

<!---
http://xahlee.info/comp/unicode_computing_symbols.html
❖ super
⇧ shift
⎇ alt
⎈ control
↹ tab
↑↓←→ arrows
⎋ escape
⎉ pause
⎊ break
--->

## Restart Graphics Driver
❖ ⎈ ⇧ B


----
----

# Open things

## Open File Explorer
❖ E

## Open Run Dialog
❖ R

## Open Settings
❖ I

## Open System Information
❖ ⎉

## Open Task Manager
⎈ ⇧ ⎋

## Open Emoji Panel
❖ .

## Open Excel
⎈ ⇧ ❖ ⎇ X

## Open Word
⎈ ⇧ ❖ ⎇ W

## Open PowerPoint
⎈ ⇧ ❖ ⎇ P


----
----

# Window management

## Task View
❖ ↹

## Task Switcher
⎇ ↹

## Move apps to different quadrants
❖ ↑↓←→

## Show/Hide the Desktop
❖ D

## Minimize all Windows
❖ M


----
----

# Screenshots

## Capture and save the whole screen
❖ ⎙

## Use Snipping Tool
❖ ⇧ S

----
----

# Virtual Desktops

## Change Virtual Desktop
❖ ⎈ ←→

## New Virtual Desktop
❖ ⎈ D

## Close Virtual Desktop
❖ ⎈ F4

