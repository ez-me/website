# Personalization

## Behavior

<!-- 🐚 -->
<a href="https://open-shell.github.io/Open-Shell-Menu/">
 Open Shell Menu</a>
<a href="https://github.com/Open-Shell/Open-Shell-Menu/releases/latest/">⬇️</a>
<br><br>

<!-- ↖️ -->
<a href="https://github.com/RamonUnch/AltSnap/">
 AltSnap</a>
<a href="https://github.com/RamonUnch/AltSnap/releases/latest/">⬇️</a>
<br><br>

<!-- 🟡 -->
<a href="https://winaerotweaker.com/">
 Winaero Tweaker</a>
<br><br>


## Cursors

<!-- ↖️ -->
<a href="https://michieldb.nl/other/cursors/">
 Posy's improved cursors</a>
<a download href="https://michieldb.nl/other/cursors/Posy's%20Cursor.zip">⬇️</a>
<br>

