# Programs

## Multiple Programs Updater/Installer

<a href="https://patchmypc.com/home-updater">
<img src="../Media/w/patchmypc.webp" alt="🖥️" loading="lazy" class="icon"> Patch My PC</a>
<a download href="https://patchmypc.com/freeupdater/PatchMyPC.exe">⬇️</a>
<br>


## Web Browsers

<a href="https://mozilla.org/firefox/">
<img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="icon"> Mozilla Firefox</a>
<a href="https://mozilla.org/firefox/all/">⬇️</a>
<br><br>

<a href="https://google.com/chrome/">
<img src="../Media/m/chrome.webp" alt="🔵" loading="lazy" class="icon"> Google Chrome</a>
<a href="https://google.com/chrome/?standalone=1#js-other-platform">⬇️</a>
<br>


## Multimedia

<a href="https://ffmpeg.org/">
<img src="../Media/w/ffmpeg.webp" alt="🟩" loading="lazy" class="icon">  FFmpeg</a>
<a download href="https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-win64-gpl.zip">⬇️</a>
<br><br>

### Audio/Video

<a href="https://videolan.org/vlc/">
<img src="../Media/w/vlc.webp" alt="🔶" loading="lazy" class="icon"> VLC media player</a>
<br><br>

<a href="https://kdenlive.org/">
<img src="../Media/w/kdenlive.webp" alt="🔷" loading="lazy" class="icon"> Kdenlive</a>
<a href="https://kdenlive.org/download/">⬇️</a>
<br><br>

<a href="https://audacityteam.org/">
<img src="../Media/w/audacity.webp" alt="🎧" loading="lazy" class="icon"> Audacity</a>
<a href="https://audacityteam.org/download/windows/">⬇️</a>
<a href="https://lame.buanzo.org/ffmpeg.php">➕</a>
<br><br>

## Pictures

<a href="https://imageglass.org/">
<img src="../Media/w/ImageGlass.webp" alt="🟦" loading="lazy" class="icon"> ImageGlass</a>
<a href="https://github.com/d2phap/ImageGlass/releases/tag/8.12.4.30">8️⃣</a>
<a href="https://github.com/d2phap/ImageGlass/releases/latest/">⬇️</a>
<br><br>

<!--
 Maybe change ImageGlass to other alternatives
 🍷 https://nomacs.org/
 🖼️ https://interversehq.com/qview/
 🌄 https://github.com/sylikc/jpegview
 Depends on ease of use and format support

 I tested them, and they are fine with format support, but they can't match the amount of ImageGlass
-->

<a href="https://getpaint.net/">
<img src="../Media/w/PaintDotNet.webp" alt="🖼️" loading="lazy" class="icon"> Paint.NET</a>
<a href="https://github.com/paintdotnet/release/releases/latest/">⬇️</a>
<br><br>

<a href="https://pinta-project.com/">
<img src="../Media/w/Pinta.webp" alt="🖼️" loading="lazy" class="icon"> Pinta</a>
<a href="https://github.com/PintaProject/Pinta/releases/latest/">⬇️</a>
<br><br>

<!---
 Maybe add GIMP and Krita?
 🎨 https://www.gimp.org/
 🖌️ https://krita.org/
--->

<a href="https://darktable.org/">
<img src="../Media/w/darktable.webp" alt="🌈" loading="lazy" class="icon"> darktable</a>
<a href="https://darktable.org/install/">⬇️</a>
<br>


## Documents

<a href="https://libreoffice.org/">
<img src="../Media/w/libreoffice.webp" alt="📄" loading="lazy" class="icon"> Libre Office</a>
<a href="https://libreoffice.org/download/download/">⬇️</a>
<br><br>

<a href="https://notepad-plus-plus.org/">
<img src="../Media/w/npp.webp" alt="📝" loading="lazy" class="icon"> Notepad++</a>
 <a href="https://github.com/notepad-plus-plus/notepad-plus-plus/releases/latest/">⬇️</a>
<br><br>

<a href="https://sumatrapdfreader.org/">
<img src="../Media/w/sumatrapdf.webp" alt="🟨" loading="lazy" class="icon"> Sumatra PDF</a>
 <a href="https://sumatrapdfreader.org/download-free-pdf-viewer">⬇️</a>
<br><br>

<a href="https://naps2.com/">
<img src="../Media/w/naps2.webp" alt="🖨️" loading="lazy" class="icon"> <abbr title="Not Another PDF Scanner 2">NAPS2</abbr></a>
<a href="https://github.com/cyanfish/naps2/releases/latest/">⬇️</a>
<br>


## Archiving

<a href="https://7-zip.org/">
<img src="../Media/w/7-zip.webp" alt="🔲" loading="lazy" class="icon"> 7-Zip</a>
<a href="https://7-zip.org/download.html">⬇️</a>
<br><br>

<a href="https://mcmilk.de/projects/7-Zip-zstd/">
<img src="../Media/w/zstd.webp" alt="🔳" loading="lazy" class="icon"> 7-Zip Zstd</a>
<a href="https://github.com/mcmilk/7-Zip-zstd/releases/latest/">⬇️</a>
<br>

<!---
 Consider PeaZip and NanaZip

 🫛 https://peazip.github.io/
 🟦 https://github.com/M2Team/NanaZip
--->


## Copying

<a href="https://fastcopy.jp/">
<img src="../Media/w/fastcopy.webp" alt="🔵" loading="lazy" class="icon"> FastCopy</a>
<a href="https://github.com/FastCopyLab/FastCopyDist2/">⬇️</a>
<br>

<!---
 <a href="https://copyhandler.com/">
  Copy Handler</a>
 <a href="https://fosshub.com/Copy-Handler.html">⬇️</a>
 <br><br>
--->


## SysInternals

### Command Line

<a href="https://docs.microsoft.com/sysinternals/downloads/junction">Junction</a>
<a download href="https://download.sysinternals.com/files/Junction.zip">⬇️</a>

<a href="https://docs.microsoft.com/sysinternals/downloads/pendmoves">PendMoves and MoveFile</a>
<a download href="https://download.sysinternals.com/files/PendMoves.zip">⬇️</a>

<a href="https://docs.microsoft.com/sysinternals/downloads/sync">Sync</a>
<a download href="https://download.sysinternals.com/files/Sync.zip">⬇️</a>

### GUI

<!-- 🟦 -->
<a href="https://docs.microsoft.com/sysinternals/downloads/autoruns">Autoruns</a>
<a download href="https://download.sysinternals.com/files/Autoruns.zip">⬇️</a>

<!-- 🟦 -->
<a href="https://docs.microsoft.com/sysinternals/downloads/notmyfault">NotMyFault</a>
<a download href="https://download.sysinternals.com/files/NotMyFault.zip">⬇️</a>

<!-- 🟦 -->
<a href="https://docs.microsoft.com/sysinternals/downloads/process-explorer">Process Explorer</a>
<a download href="https://download.sysinternals.com/files/ProcessExplorer.zip">⬇️</a>

<!-- 🌈 -->
<a href="https://docs.microsoft.com/sysinternals/downloads/rammap">RAMMap</a>
<a download href="https://download.sysinternals.com/files/RAMMap.zip">⬇️</a>

# Old

<a href="https://win7games.com/">
<img src="../Media/w/win7games.webp" alt="🎮" loading="lazy" class="icon"> Windows 7 Games (and more)</a>
<br><br>

<!-- 🗒️ -->
<a href="https://fdossena.com/?p=stickynotes/i.frag">
 NoteBot</a>
<a href="https://github.com/adolfintel/NoteBot/releases/latest/">⬇️</a>
<br>


## Other

<a href="https://rejzor.wordpress.com/permanent-process-priority-tool/">
<img src="../Media/w/Crystal128.webp" alt="⚙️" loading="lazy" class="icon"> Permanent Process Priority Tool</a>
<a download href="https://filedn.eu/lUQWGE4HgtC4NVKJo4uUeKX/Permanent%20Process%20Priority%20Tool/PPPT.zip">⬇️</a>
<br><br>

<!-- 💽 -->
<a href="https://hdtune.com/">
 HD Tune</a>
<a download href="https://hdtune.com/files/hdtune_255.exe">⬇️</a>
<br><br>

<!-- 🔵 -->
<a href="https://www.romexsoftware.com/en-us/primo-cache/">
 PrimoCache</a> (Paid)
<a href="https://www.romexsoftware.com/en-us/primo-cache/download.html">⬇️</a>
<br><br>

<!-- Ⓜ️ -->
<a href="https://msys2.org/">
 MSYS2</a>
<a href="https://msys2.org/#installation">⬇️</a>
<br>

