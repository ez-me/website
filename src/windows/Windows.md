# Windows

- [System](./system.md)
- [Repair](./repair.md)
- [Programs](./programs.md)
- [PowerShell Downloads](./powershell-downloads.md)
- [Gaming Fixes](./gaming.md)
- [Personalization](./personalization.md)
- [Integrated Tools](.integrated.md)
- [Shortcuts](./shortcuts.md)
- [ISO](./iso.md)
- [Activation](./activation.md)
