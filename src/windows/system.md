# System Modifications

## Runtimes and Libraries

<a href="https://github.com/abbodi1406/vcredist/">
<img src="../Media/w/msvcpp.webp" alt="🌐" loading="lazy" class="icon"> Visual C++ Redist (AIO)</a>
<a href="https://github.com/abbodi1406/vcredist/releases/latest/">⬇️</a>
<br><br>

<!---
Microsoft XNA Framework Redistributable 4.0
https://microsoft.com/download/details.aspx?id=20914
--->

<a href="https://developer.microsoft.com/microsoft-edge/webview2/">
<img src="../Media/w/edge.webp" alt="🟢" loading="lazy" class="icon"> Edge WebView2</a>
<a href="https://developer.microsoft.com/microsoft-edge/webview2#download">⬇️</a>
<br><br>

<a href="https://codecguide.com/">
<img src="../Media/w/mpc.webp" alt="🎬" loading="lazy" class="icon"> K-Lite Codec Pack</a>
<a href="https://majorgeeks.com/files/details/k_lite_codec_pack_basic.html">⬇️</a>
<br><i>Basic doesn't have a video player (we use VLC).</i>
<br><br>

<a href="https://www.azul.com/">
 Azul Zulu (Java)</a>
<a href="https://www.azul.com/downloads/?version=java-21-lts&os=windows&architecture=x86-64-bit&package=jre-fx#zulu">⬇️</a>
<br>

<!---
<a href="https://github.com/zlib-ng/zlib-ng/">
 zlib-ng</a>
<a href="https://github.com/zlib-ng/zlib-ng/releases/latest/">⬇️</a>
<br><br>

<a href="https://github.com/mozilla/mozjpeg/">
 MozJPEG</a>
<a href="https://github.com/dofuuz/mozjpeg/releases/latest/">⬇️</a>
<br><br>

--->

### dot NET

<a href="https://dotnet.microsoft.com/download/dotnet/6.0">
<img src="../Media/w/dotnet.webp" alt="🌐" loading="lazy" class="icon"> .NET 6</a>
<br><br>

<a href="https://dotnet.microsoft.com/download/dotnet/8.0">
<img src="../Media/w/dotnet.webp" alt="🌐" loading="lazy" class="icon"> .NET 8</a>
<br><br>

<!---
<a href="https://dotnet.microsoft.com/download/dotnet/9.0">
<img src="../Media/w/dotnet.webp" alt="🌐" loading="lazy" class="icon"> .NET 9</a>
<br><br>
--->

<!--- Is this one really needed? 
<a href="https://dotnet.microsoft.com/download/dotnet-framework/net48">
<img src="../Media/w/netfx.webp" alt="🌐" loading="lazy" class="icon"> .NET Framework 4.8</a>
<a href="https://dotnet.microsoft.com/download/dotnet-framework/thank-you/net48-offline-installer">⬇️</a>
<br><br>
--->

<a href="https://mono-project.com/">
<img src="../Media/w/mono.webp" alt="🐵" loading="lazy" class="icon"> Mono</a>
<a href="https://mono-project.com/download/stable/#download-win">⬇️</a>
<br>


### DirectX

<a href="https://microsoft.com/download/8109">
<img src="../Media/w/directx11.webp" alt="✖️" loading="lazy" class="icon"> DirectX Runtimes June 2010</a>
<a href="https://microsoft.com/download/confirmation.aspx?id=8109">⬇️</a>
<br>


## Windows 10 and 11

<a href="https://wpd.app/">
<img src="../Media/w/wpd.webp" alt="🛸" loading="lazy" class="icon"> Windows Privacy Dashboard</a>
<a download="wpd.zip" href="https://wpd.app/get/latest.zip">⬇️</a>
<br><br>

<a href="https://oo-software.com/en/shutup10">
<img src="../Media/w/shutup10.webp" alt="📢" loading="lazy" class="icon"> O&O ShutUp10++</a>
<a download="ShutUp10.exe" href="https://dl5.oo-software.com/files/ooshutup10/OOSU10.exe">⬇️</a>
<br><br>

<a href="https://privacy.sexy/">
<img src="../Media/w/privacy-sexy.webp" alt="👁️‍🗨️" loading="lazy" class="icon"> Privacy Sexy</a>
<br><br>

<!---
<a href="https://github.com/builtbybel/privatezilla/">
<img src="../Media/w/privatezilla.webp" alt="🌐" loading="lazy" class="icon"> Privatezilla</a>
<a href="https://github.com/builtbybel/privatezilla/releases/latest/">⬇️</a>
<br>
--->


## Drivers

### Common

<a href="https://sdi-tool.org/">
<img src="../Media/w/sdi.webp" alt="🌐" loading="lazy" class="icon"> Snappy Driver Installer</a>
<a href="https://sdi-tool.org/downloadlite/">⬇️</a>
<br><i>Choose to download only indexes, then install all. Or use the torrent.</i>
<br><br>

<a href="https://www.nvidia.com/drivers/">
<img src="../Media/w/nvidia.webp" alt="👁️" loading="lazy" class="icon"> Manual NVIDIA Driver Search</a>
<a href="https://www.techpowerup.com/download/nvidia-geforce-graphics-drivers/">🪞</a>
<br><br>

<a href="https://amd.com/support">
<img src="../Media/w/amd.webp" alt="🔲" loading="lazy" class="icon"> AMD Drivers</a>
<a href="https://www.techpowerup.com/download/amd-radeon-graphics-drivers/">🪞</a>
<br><br>

<a href="https://intel.com/content/www/us/en/support/detect.html">
<img src="../Media/w/intel.webp" alt="🔵" loading="lazy" class="icon"> Intel Driver & Support Assistant</a>
<a download href="https://dsadata.intel.com/installer">⬇️</a>
<a href="https://www.techpowerup.com/download/intel-graphics-drivers/">🪞</a>
<br>

### Other

<a href="https://adb.clockworkmod.com/">
<img src="../Media/w/adb.webp" alt="🟢" loading="lazy" class="icon"> Universal ADB Drivers</a>
<a download href="https://github.com/koush/adb.clockworkmod.com/releases/latest/download/UniversalAdbDriverSetup.msi">⬇️</a>
<br><br>

<a href="https://github.com/maharmstone/btrfs/">
 btrfs - B Tree FileSystem</a>
<a href="https://github.com/maharmstone/btrfs/releases/latest/">⬇️</a>
<br><br>

<a href="https://www.bluetoothgoodies.com/a2dp/">
<img src="../Media/w/a2dp.webp" alt="🔊" loading="lazy" class="icon">
 Alternative A2DP Driver</a> (Paid)
<br><br>

<a href="https://zadig.akeo.ie/">
<img src="../Media/w/zadig.webp" alt="🇿" loading="lazy" class="icon"> Zadig</a>
<a href="https://github.com/pbatard/libwdi/releases/latest/">⬇️</a>
<br><br>


