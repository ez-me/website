# PowerShell Downloads

## General

<img src="../Media/w/patchmypc.webp" alt="🖥️" loading="lazy" class="icon"> Patch My PC
```ps
irm https://ezme.neocities.org/ps/patchmypc | iex
```


<img src="../Media/m/firefox.webp" alt="🦊" loading="lazy" class="icon"> Mozilla Firefox

```ps
irm https://ezme.neocities.org/ps/firefox | iex
```


<img src="../Media/w/zstd.webp" alt="🔳" loading="lazy" class="icon"> 7-Zip Zstd

```ps
irm https://ezme.neocities.org/ps/7zip | iex
```

## Remote Desktop

 DwService

```ps
irm https://ezme.neocities.org/ps/dwservice | iex
```

 RustDesk

```ps
irm https://ezme.neocities.org/ps/rustdesk | iex
```

 UltraViewer

```ps
irm https://ezme.neocities.org/ps/ultraviewer | iex
```

