# ISO Modification

## Download ISO, updates

### Official

<a href="https://microsoft.com/software-download/windows10ISO">
<img src="../Media/w/w10.webp" alt="🪟" loading="lazy" class="icon"> Windows 10</a>
<br>
<i>If you use Windows to access the website, it will redirect to the Download Tool; If you just want the ISO file, use another OS or spoof the User Agent.</i>
<br><br>

<a href="https://microsoft.com/software-download/windows11">
<img src="../Media/w/w11.webp" alt="🪟" loading="lazy" class="icon"> Windows 11</a>
<br><br>

<a href="https://catalog.update.microsoft.com/Home.aspx">
<img src="../Media/w/update-catalog.webp" alt="🛒" loading="lazy" class="icon"> MS Update Catalog</a>
<br>


### Alternative

<a href="https://massgrave.dev/windows_10_links#download-links">
 Massgrave Windows 10 Links</a>
<br><br>

<a href="https://massgrave.dev/windows_11_links#download-links">
 Massgrave Windows 11 Links</a>
<br><br>

<a href="https://files.rg-adguard.net/category">
<img src="../Media/w/rg.webp" alt="🪟" loading="lazy" class="icon"> File list</a>
<br><br>



### Custom ISO

<!---
https://archive.org/details/@ntdev
https://ntdev.blog/2024/01/08/the-complete-tiny10-and-tiny11-list/
--->

<a href="https://youtube.com/watch?v=4cF3UW5W980">
 Tiny10</a>
<a download href="https://archive.org/download/tiny-10-23-h2/tiny10%20x64%2023h2.iso">⬇️</a>
<br><br>

<a href="https://youtube.com/watch?v=Y8YIadhWbho">
 Tiny11</a>
<a download href="https://archive.org/download/tiny11-2311/tiny11%202311%20x64.iso">⬇️</a>
<br>


### Custom PE

<a href="https://gbatemp.net/threads/release-jayros-lockpick-a-bootable-password-removal-suite-winpe.579278/">
 Jayro's Lockpick</a>
<a download href="https://archive.org/download/jayros-lockpick-x86-21.12/Jayros%20Lockpick%20x86%20%2821.12%29.iso">⬇️</a>
<br><br>

<a href="https://archive.org/download/bob-ombs-windows-10-pe-iso/Bob.Ombs.Modified.Win10PEx64.v4.985.ISO">
 Bob.Omb's Modified Win10PE</a>
<br>


## Tools for modifying

<a href="https://msmgtoolkit.in/">
 MSMG Toolkit</a>
<br><br>

<a href="https://github.com/Legolash2o/WinToolkit_v1/">
<img src="../Media/w/wintoolkit.webp" alt="🌐" loading="lazy" class="icon"> WinToolKit</a>
<a download href="https://github.com/Legolash2o/WinToolkit_v1/raw/master/WTK_v1_1.7.0.1.7z">⬇️</a>
<br><br>

<a href="https://ntlite.com/">
<img src="../Media/w/ntlite.webp" alt="🌐" loading="lazy" class="icon"> NT Lite</a>
<br>


## Pendrive Managers

### Single

<a href="https://rufus.ie/">
 Rufus</a>
<a href="https://github.com/pbatard/rufus/releases/latest/">⬇️</a>
<br>

### Multiple

<a href="https://ventoy.net/">
 Ventoy</a>
<a href="https://github.com/ventoy/Ventoy/releases/latest/">⬇️</a>
<br>

