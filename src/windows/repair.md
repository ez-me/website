# Repair

## General

<a href="https://guru3d.com/files-details/display-driver-uninstaller-download.html">
<img src="../Media/w/ddu.webp" alt="🌐" loading="lazy" class="icon"> Display Driver Uninstaller</a>
<br><br>

<a href="https://rejzor.wordpress.com/powerdefrag/">
<img src="../Media/w/defrag.webp" alt="🌐" loading="lazy" class="icon"> Power Defragmenter</a>
<a download href="https://filedn.eu/lUQWGE4HgtC4NVKJo4uUeKX/Downloads/PowerDefragmenter.zip">⬇️</a>
<br><br>

<a href="https://support.microsoft.com/office/uninstall-office-automatically-9ad57b43-fa12-859a-9cf0-b694637b3b05">
<img src="../Media/w/sara.webp" alt="🌐" loading="lazy" class="icon"> Uninstall MS Office</a>
<a href="https://aka.ms/SaRA-OfficeUninstallFromPC">⬇️</a>
<br><br>

### Cleaning

<a href="https://bleachbit.org/">
<img src="../Media/w/bleachbit.webp" alt="🌐" loading="lazy" class="icon"> BleachBit</a>
<a href="https://bleachbit.org/download/windows/">⬇️</a>
<br><br>

<a href="https://github.com/builtbybel/burnbytes/">
<img src="../Media/w/burnbytes.webp" alt="🌐" loading="lazy" class="icon"> Burnbytes</a>
<a download href="https://github.com/builtbybel/burnbytes/releases/download/0.12.2/Burnbytes.exe">⬇️</a>
<br><br>

<a href="http://chuyu.me/en-US/">
<img src="../Media/w/dism++.webp" alt="🌐" loading="lazy" class="icon"> Dism++</a>
<a download href="https://github.com/Chuyu-Team/Dism-Multi-language/releases/download/v10.1.1002.2/Dism++10.1.1002.1B.zip">⬇️</a>
<br>

### Windows Update

<a href="https://thewindowsclub.com/repair-fix-windows-updates-with-fix-wu-utility">
<img src="../Media/w/fix-wu.webp" alt="🌐" loading="lazy" class="icon"> Fix WU Utility</a>
<a download href="https://thewindowsclub.com/downloads/Fix%20WU.zip">⬇️</a>
<br><br>

<a href="https://rejzor.wordpress.com/2016/11/11/windows-update-cache-cleaner/">
<img src="../Media/w/Crystal128.webp" alt="🌐" loading="lazy" class="icon"> Windows Update Cache Cleaner</a>
<a download href="https://filedn.eu/lUQWGE4HgtC4NVKJo4uUeKX/Downloads/WindowsUpdate_Cache_Cleaner.7z">⬇️</a>
<br>

### Blue Screen of Death

<a href="https://www.nirsoft.net/utils/blue_screen_view.html">
<img src="../Media/w/nir-bsod.webp" alt="🌐" loading="lazy" class="icon"> BlueScreenView</a>
<a download href="https://www.nirsoft.net/utils/bluescreenview_setup.exe">⬇️</a>
<br>



## Anti Malware

### Portable

<a href="https://toolslib.net/downloads/viewdownload/1-adwcleaner/">
<img src="../Media/w/adwcleaner.webp" alt="🌐" loading="lazy" class="icon"> AdwCleaner</a>
<a href="https://toolslib.net/downloads/finish/1-adwcleaner/">⬇️</a>
<br><br>

<a href="https://emsisoft.com/home/emergencykit/">
<img src="../Media/w/eek.webp" alt="🌐" loading="lazy" class="icon"> Emsisoft Emergency Kit</a>
<a download href="https://dl.emsisoft.com/EmsisoftEmergencyKit.exe">⬇️</a>
<br><br>

<a href="https://kaspersky.com/downloads/free-virus-removal-tool">
<img src="../Media/w/kvrt.webp" alt="🌐" loading="lazy" class="icon"> Kaspersky Virus Removal Tool</a>
<a download href="https://devbuilds.s.kaspersky-labs.com/devbuilds/KVRT/latest/full/KVRT.exe">⬇️</a>
<br><br>

<a href="https://support.norton.com/sp/static/external/tools/npe.html">
<img src="../Media/w/npe.webp" alt="🌐" loading="lazy" class="icon"> Norton Power Eraser</a>
<a href="https://norton.com/npe_latest">⬇️</a>
<br><br>

<a href="https://bleepingcomputer.com/download/rkill/">
<img src="../Media/w/rkill.webp" alt="🌐" loading="lazy" class="icon"> RKill</a>
<a href="https://bleepingcomputer.com/download/rkill/dl/10/">⬇️</a>
<br>


### Rootkit

<a href="http://www.gmer.net/?m=0">
<img src="../Media/w/gmer.webp" alt="🌐" loading="lazy" class="icon"> GMER</a>
<a download href="http://www2.gmer.net/gmer.zip">⬇️</a>
<br><br>

<a href="https://usa.kaspersky.com/content/custom/global/tdsskiller/tdsskiller.html">
<img src="../Media/w/kvrt.webp" alt="🌐" loading="lazy" class="icon"> TDSSKiller</a>
<a download href="https://www.majorgeeks.com/mg/getmirror/kaspersky_tdsskiller,1.html">⬇️</a>
<br>


## Instalable

<a href="https://bitdefender.com/solutions/free.html">
<img src="../Media/w/bitdefender.webp" alt="🌐" loading="lazy" class="icon"> Bitdefender Free</a>
<a download href="https://download.bitdefender.com/windows/installer/en-us/bitdefender_avfree.exe">⬇️</a>
<br>
<i>You can use temporary email if you don't want to use your own.</i>
<br><br>

<a href="https://emsisoft.com/">
<img src="../Media/w/emsisoft.webp" alt="🌐" loading="lazy" class="icon"> Emsisoft Anti-Malware</a> (Paid)
<a download href="https://dl.emsisoft.com/EmsisoftAntiMalwareWebSetup.exe">⬇️</a>
<br>


### Ransomware

<a href="https://kaspersky.com/anti-ransomware-tool">
<img src="../Media/w/kart.webp" alt="🌐" loading="lazy" class="icon"> Kaspersky Anti Ransomware Tool</a>
<a download href="https://kas.pr/kart-home">⬇️</a>
<br>


