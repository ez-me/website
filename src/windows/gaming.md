# Gaming fixes


## Other

<a href="https://www.special-k.info/">
 Special K</a>
<a href="https://github.com/SpecialKO/SpecialK/releases/latest/">⬇️</a>
<br><br>

<a href="https://msi.com/Landing/afterburner/graphics-cards">
<img src="../Media/w/msiafterburner.webp" alt="🚀" loading="lazy" class="icon"> MSI Afterburner</a>
<a href="https://www.guru3d.com/download/msi-afterburner-beta-download/">🪞</a>
<br><br>

<a href="https://guru3d.com/download/rtss-rivatuner-statistics-server-download/">
 Rivatuner Statistics Server</a>
<br><br>

<a href="https://store.steampowered.com/app/993090/Lossless_Scaling/">
 Lossless Scaling</a> (Paid)
<br><br>

<a href="https://mion.yosei.fi/BES/">
 Battle Encoder Shirase</a>
<br>


## Input Remapping

|Program|Physical|Emulated|
|---|---|---|
|<a href="https://github.com/Snoothy/UCR/"><img src="../Media/w/ucr.webp" alt="⬛" loading="lazy" class="icon"> Universal Control Remapper</a><a href="https://github.com/Snoothy/UCR/releases/latest/">⬇️</a>|Any|Any|
|<a href="https://github.com/AntiMicroX/antimicroX/"><img src="../Media/w/antimicrox.webp" alt="🎮" loading="lazy" class="icon"> AntiMicroX</a><a href="https://github.com/AntiMicroX/antimicroX/releases/latest/">⬇️</a>|Any Gamepad|Keyboard, Mouse|
|<a href="https://github.com/Electronicks/JoyShockMapper/"><img src="../Media/w/joyshockmapper.webp" alt="🎮" loading="lazy" class="icon"> JoyShockMapper</a><a href="https://github.com/Electronicks/JoyShockMapper/releases/latest/">⬇️</a>|Any Gamepad with Gyroscope|Keyboard, Mouse, Gamepad|
|<a href="https://github.com/djlastnight/KeyboardSplitterXbox/">Keyboard Splitter</a><a href="https://github.com/djlastnight/KeyboardSplitterXbox/releases/latest/">⬇️</a>|<abbr title="Up to 10 different keyboards">Keyboard</abbr>|<abbr title="Up to 4 different controllers">Xbox Controller</abbr>|



## 3dfx / Glide

|Program|Glide|Wrapper|
|---|---|---|
|<a href="http://dege.freeweb.hu/dgVoodoo2/"><img src="../Media/w/dgVoodoo2.webp" alt="🟧" loading="lazy" class="icon"> dgVooDoo2</a><a href="https://github.com/dege-diosg/dgVoodoo2/releases/latest/">⬇️</a>|1-3|D3D 11,12|
|<a href="https://www.zeus-software.com/downloads/nglide/">nGLide</a><a href="https://www.zeus-software.com/downloads/nglide/d-1.html">⬇️</a>|1-3|D3D 9,<br>Vulkan|


## DirectX

### DirectDraw

|Program|DDraw|Wrapper|
|---|---|---|
|<a href="https://github.com/narzoul/DDrawCompat/">DDrawCompat</a><a href="https://github.com/narzoul/DDrawCompat/releases/">⬇️</a>|1-7|Optimized DDraw 1-7|
|<a href="http://dege.freeweb.hu/dgVoodoo2/"><img src="../Media/w/dgVoodoo2.webp" alt="🟧" loading="lazy" class="icon"> dgVooDoo2</a><a href="https://github.com/dege-diosg/dgVoodoo2/releases/latest/">⬇️</a>|1-7|D3D 11,12|
|<a href="https://github.com/FunkyFr3sh/cnc-ddraw/">CNC DDraw</a><a href="https://github.com/FunkyFr3sh/cnc-ddraw/releases/latest/">⬇️</a>|1-7|GDI, D3D 9,<br>OpenGL|
|<a href="https://github.com/elishacloud/dxwrapper/">DxWrapper</a><a href="https://github.com/elishacloud/dxwrapper/releases/latest/download/dxwrapper.zip">⬇️</a>|1-7|Optimized DDraw 7,<br>D3D 9|

<i>If using Windows 8 or newer, apply [this registry tweak](https://raw.githubusercontent.com/narzoul/DDrawCompat/master/Tools/InstallDDrawCOMRedirection.reg).</i>
<br>


### Direct3D

<a href="https://legoisland.org/wiki/index.php?title=Direct3D_Retained_Mode">
 D3D Retained Mode</a>
<a download href="https://legoisland.org/download/d3drm.zip">⬇️</a>
<br><br>

<a href="https://sourceforge.net/projects/dxwnd/">
 DxWnd</a>
<a download href="https://sourceforge.net/projects/dxwnd/files/Latest%20build/">⬇️</a>
<br><br>

<a href="https://github.com/tuffee88/d3d12ProxyEdrDx11_0/">
 d3d12ProxyEdrDx11_0</a>
<a download href="https://github.com/tuffee88/d3d12ProxyEdrDx11_0/releases/latest">⬇️</a>
<br>

|Program|Direct3D|Wrapper|
|---|---|---|
|<a href="https://github.com/elishacloud/dxwrapper/">DxWrapper</a><a href="https://github.com/elishacloud/dxwrapper/releases/latest/download/dxwrapper.zip">⬇️</a>|2-8|D3D 9|
|<a href="http://dege.freeweb.hu/dgVoodoo2/"><img src="../Media/w/dgVoodoo2.webp" alt="🟧" loading="lazy" class="icon"> dgVooDoo2</a><a href="https://github.com/dege-diosg/dgVoodoo2/releases/latest/">⬇️</a>|2-9|D3D 10-12|
|<a href="https://fdossena.com/?p=wined3d/index.frag"><img src="../Media/w/wine.webp" alt="🍷" loading="lazy" class="icon"> WineD3D</a><a download href="https://downloads.fdossena.com/geth.php?r=wined3dst-latest">⬇️</a>|2-11|OpenGL /<br>[Vulkan](https://gitlab.com/ez-me/settings/-/raw/main/windows/registry/WineD3D%20Vulkan.reg?inline=false)|
|<a href="https://github.com/doitsujin/dxvk/">DXVK</a><a href="https://github.com/doitsujin/dxvk/releases/latest/">⬇️</a>|8-11|Vulkan|
|<a href="https://gitlab.com/Ph42oN/dxvk-gplasync/">dxvk-gplAsync</a><a href="https://gitlab.com/Ph42oN/dxvk-gplasync/-/releases/">⬇️</a>|8-11|Vulkan|
|<a href="https://github.com/HansKristian-Work/vkd3d-proton/">VKD3D-proton</a><a href="https://github.com/HansKristian-Work/vkd3d-proton/releases/latest/">⬇️</a>|12|Vulkan|


### DInput / XInput

<a href="https://community.pcgamingwiki.com/files/file/58-microsoft-directinput-mapper">
 DirectInput Mapper</a>

|Program|Original|Wrapper|
|---|---|---|
|<a href="https://github.com/mitigd/dinput8-wrapper-fps-fix/">DirectInput8 FPS Fix</a><a href="https://github.com/mitigd/dinput8-wrapper-fps-fix/releases/latest/">⬇️</a>|DInput 8|Optimized DInput 8|
|<a href="https://github.com/MrShoor/DInput8_CacheFix/">DirectInput8 Cache Fix</a><a href="https://github.com/MrShoor/DInput8_CacheFix/releases/latest/">⬇️</a>|DInput 8|Optimized DInput 8|
|<a href="https://github.com/geeky/dinput8wrapper/">dinput8wrapper</a><a href="https://github.com/geeky/dinput8wrapper/releases/latest/">⬇️</a>|DInput 8|<abbr title="Only Keyboard and Mouse">Raw Input</abbr>|
|<a href="https://github.com/elishacloud/dinputto8/">dinputto8</a><a href="https://github.com/elishacloud/dinputto8/releases/latest/">⬇️</a>|DInput 1-7|DInput 8|
|<a href="https://github.com/elishacloud/dxwrapper/">DxWrapper</a><a href="https://github.com/elishacloud/dxwrapper/releases/latest/download/dxwrapper.zip">⬇️</a>|DInput 1-7|DInput 8|
|<a href="https://github.com/samuelgr/Xidi/">Xidi</a><a href="https://github.com/samuelgr/Xidi/releases/latest/">⬇️</a>|XInput|DInput 1-8,<br>WinMM|
|<a href="https://x360ce.com/">x360ce</a><a download href="https://x360ce.com/files/x360ce.zip">⬇️</a>|DInput 1-8|XInput|


### DirectSound

<a href="https://www.bockholdt.com/dsc/index2.html">
 DirectSound Control</a>
<a download href="http://www.bockholdt.com/dsc/dsc.zip">⬇️</a>

|Program|Original|Wrapper|
|---|---|---|
|<a href="https://github.com/kcat/dsoal/">DSOAL</a><a download href="http://vaporeon.io/hosted/dsoal-builds/dsoal-latest.7z">⬇️</a>|DSound + EAX|OpenAL Soft|
|<a href="https://www.indirectsound.com/">IndirectSound</a><a href="https://www.indirectsound.com/downloads.html">⬇️</a>|DSound|XAudio2|

<i>If using Windows 8 or newer, apply [this registry tweak](https://gitlab.com/ez-me/settings/-/raw/main/windows/registry/DirectSound.reg?inline=false).</i>
<br>


## XAudio

<a href="https://github.com/ThreeDeeJay/x3daudio1_7_hrtf/">
 X3DAudio HRTF</a>
<a href="https://github.com/ThreeDeeJay/x3daudio1_7_hrtf/releases/latest/">⬇️</a>

<i>If using Windows 8 or newer, apply [this registry tweak](https://gitlab.com/ez-me/settings/-/raw/main/windows/registry/XAudio2_7.reg?inline=false).</i>
<br>

<!---
I don't know if this is useful, but I'll leave it in this comment just in case
https://fna-xna.github.io/
https://fna.flibitijibibo.com/archive/fnalibs.tar.bz2
https://github.com/Kron4ek/FAudio-Builds/releases/download/20.07/faudio-20.07.tar.xz
--->


## Other Audio

<a href="https://un4seen.com/bass.html">
<img src="../Media/w/bass.webp" alt="🌐" loading="lazy" class="icon"> BASS</a>
<a download href="https://un4seen.com/files/bass24.zip">⬇️</a>
<br><br>

<a href="https://openal-soft.org/">
 OpenAL Soft</a>
<a href="https://openal-soft.org/openal-binaries/?C=N;O=D">⬇️</a>
<br>


## DLSS / FSR

<a href="https://techpowerup.com/download/nvidia-dlss-dll/">
<img src="../Media/w/nvidia.webp" alt="👁️" loading="lazy" class="icon"> NVIDIA DLSS DLL</a>
<br><br>

<a href="https://www.techpowerup.com/download/nvidia-dlss-3-frame-generation-dll/">
<img src="../Media/w/nvidia.webp" alt="👁️" loading="lazy" class="icon"> NVIDIA DLSS Frame Generation DLL</a>
<br><br>

<a href="https://www.techpowerup.com/download/nvidia-dlss-3-ray-reconstruction-dll/">
<img src="../Media/w/nvidia.webp" alt="👁️" loading="lazy" class="icon"> NVIDIA DLSS Ray Reconstruction DLL</a>
<br><br>

<a href="https://github.com/cdozdil/OptiScaler/">
 OptiScaler</a>
<a href="https://github.com/cdozdil/OptiScaler/releases/latest/">⬇️</a>
<br>


