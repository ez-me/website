# Activation

|Program|Windows|Office|
|---|---|---|
|<a href="https://massgrave.dev/">Microsoft Activation Scripts</a> <a href="https://download-directory.github.io/?url=https%3A%2F%2Fgithub.com%2Fmassgravel%2FMicrosoft-Activation-Scripts%2Ftree%2Fmaster%2FMAS%2FAll-In-One-Version-KL">⬇️</a><a download href="https://git.activated.win/massgrave/Microsoft-Activation-Scripts/raw/branch/master/MAS/All-In-One-Version-KL/MAS_AIO.cmd">⬇️</a> |HWID,<br>KMS38,<br>Public Server|Ohook,<br>Public Server|
|<a href="https://github.com/abbodi1406/KMS_VL_ALL_AIO/">KMS VL ALL AIO</a> <a download href="https://raw.githubusercontent.com/abbodi1406/KMS_VL_ALL_AIO/master/KMS_VL_ALL_AIO.cmd">⬇️</a>|KMS38,<br>Local server|Local server|

