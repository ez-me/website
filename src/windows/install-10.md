# Install Windows 10

## Choosing the right version

The Home and Pro versions aren't really the best, what you really want is the Education or LTSC editions.

Benefits include the Group Policy Editor, which allows you to control advanced parts of Windows manually, and disabling most tracking.

Use Education if you want to use the Store and/or Gamebar, but go for LTSC if you don't need them.


## Installation (part 1)

Very importantly, unplug any Ethernet cables, or USB tethers, and do not connect to any WiFi hotspots.

Choose the proper Time and Currency format according to where you live, and the Keyboard based on what you physically have.

Hit <code>Install Now</code>

Select <code>I don't have a product key</code>, or maybe if you already have one just type it down (Generic ones work too).

Depending on your ISO, either choose the intended version that shows up, or you'll be sent to the License Terms immediatly.
Just "accept" them and hit <code>Custom: Install Windows only (advanced)</code>.

In the Drive selection screen, <code>Delete</code> all, hit <code>New</code> and <code>Apply</code> then <code>OK</code>.

Now in the Primary partition we want to do something special, and for that we need CMD, which thankfully can be opened with Shift F10

```cmd
diskpart

select disk=0

select partition=2

active

format fs=NTFS LABEL="OS" quick compress

exit
```

Now close the CMD window and hit <code>Back</code> then <code>Custom: Install Windows only (advanced)</code> again and <code>Next</code> to have Windows Install itself
(go do something else while it does it's thing)

## Installation (part 2)

Once you boot into the Out of Box Experience, it will prompt you to confirm your regional settings.

Hit <code>Yes</code> if everything looks correct, and <code>Skip</code> if you only need one Keyboard Layout.

Then you have to choose <code>I don't have internet</code>, then <code>Continue with limited setup</code>.

Now you can insert your user name, and you can optionally skip the password by not inputting anything and hitting <code>Next</code>.

In the privacy settings, disable everything and hit <code>Accept</code>.

