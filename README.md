mdBook [v0.4.41](https://github.com/rust-lang/mdBook/releases/tag/v0.4.41) changed how the sidebar works, with toc.html and toc.js being used, however they don't work on Neocities

I use neocities, so gotta use [v0.4.40](https://github.com/rust-lang/mdBook/releases/tag/v0.4.40)

```bash
git clone --depth=1 https://gitlab.com/ez-me/website.git && cd website

mdbook build
```
